module Test.CucumberExpression where

import Prelude

import Control.Monad.Except (runExcept)
import Data.Either (Either(..))
import Data.Int as Int
import Data.Maybe (fromMaybe)
import Effect.Aff (Aff)
import Foreign as Foreign
import Gherkin.Expression.CucumberExpression.Types (CukeExpression, CukeToken(..)) 
import Gherkin.Expression.CucumberExpression (compile, match) as CX
import Gherkin.Expression.CucumberExpression.Types (Any, CukeToken, anyToForeign, toAny) as CX
import Gherkin.Expression.CucumberExpression.Token as CXT
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (fail, shouldEqual)

shouldCompile :: String -> Array (CX.CukeToken) -> (CukeExpression -> Aff Unit) -> Aff Unit
shouldCompile str tokens f = do
    case CX.compile tokens str of
        Left e -> fail $ "Failed to compile with: " <> e
        Right ex -> f ex

shouldMatch :: CukeExpression -> String -> (Array CX.Any -> Aff Unit) -> Aff Unit
shouldMatch ex str f = do
    case CX.match ex str of
        Left e -> fail $ "Failed to invoke: " <> e
        Right res -> f res

spec :: Spec Unit
spec = describe "cucumber expression" do
    it "should match an exact string" do
        shouldCompile "hello world" [] \ex ->
            shouldMatch ex "hello world" (const $ pure unit)
    
    it "should match a token" do
        let token = CukeToken {
                name: "string",
                regex: "\\w+",
                transform: CX.toAny
            }
            
        shouldCompile "hello {string} world" [token] \ex ->
            shouldMatch ex "hello str world" \matches -> do
                let m = runExcept <$> (Foreign.readString <<< CX.anyToForeign) <$> matches
                m `shouldEqual` [Right "str"]
    
    it "should transform multiple tokens" do
        let tokenInt = CukeToken {
                name: "int",
                regex: "\\d+",
                transform: CX.toAny <<< fromMaybe 0 <<< Int.fromString
            }
        let tokenString = CukeToken {
                name: "string",
                regex: "\\w+",
                transform: CX.toAny
            }
    
        shouldCompile "I got {int} beautiful {string}" [tokenInt, tokenString] \ex ->
            shouldMatch ex "I got 10 beautiful cucumbers" \matches -> do
                case matches of
                    [int, string] -> do
                        let i = runExcept $ Foreign.readInt $ CX.anyToForeign int
                        let s = runExcept $ Foreign.readString $ CX.anyToForeign string
                        i `shouldEqual` (Right 10)
                        s `shouldEqual` (Right "cucumbers")

                    _ -> fail "missing matches"
    
    it "should escape a token" do
      shouldCompile "hello \\{string} world" [] \ex ->
            shouldMatch ex "hello {string} world" (const $ pure unit)
    
    it "should parse optional text" do
        shouldCompile "this is optional text(ual content)" [] \ex -> do
            shouldMatch ex "this is optional text" (const $ pure unit)
            shouldMatch ex "this is optional textual content" (const $ pure unit)

    it "should escape optional text" do
        shouldCompile "I need to escape \\(this) to get that" [] \ex ->
            shouldMatch ex "I need to escape (this) to get that" (const $ pure unit)

    it "should parse alternatives" do
        shouldCompile "I can offer you cucumber/strawberry" [] \ex -> do
            shouldMatch ex "I can offer you cucumber" (const $ pure unit)
            shouldMatch ex "I can offer you strawberry" (const $ pure unit)
    
    it "should parse more than two alternatives" do
          shouldCompile "I can offer you cucumber/strawberry/pineapple/orange" [] \ex -> do
            shouldMatch ex "I can offer you pineapple" (const $ pure unit)
            shouldMatch ex "I can offer you orange" (const $ pure unit)
    
    describe "default tokens" do
        it "should match a word" do
            shouldCompile "I have {word} to say" [CXT.word] \ex -> do
                shouldMatch ex "I have nothing to say" \matches -> do
                    let m = runExcept <$> (Foreign.readString <<< CX.anyToForeign) <$> matches
                    m `shouldEqual` [Right "nothing"]
        
        it "should match a double-quoted string" do
            shouldCompile "I have {string} to say" [CXT.string] \ex -> do
                shouldMatch ex "I have \"much more\" to say" \matches -> do
                    let m = runExcept <$> (Foreign.readString <<< CX.anyToForeign) <$> matches
                    m `shouldEqual` [Right "much more"]
        
        it "should match a single-quoted string" do
            shouldCompile "I have {string} to say" [CXT.string] \ex -> do
                shouldMatch ex "I have 'much more' to say" \matches -> do
                    let m = runExcept <$> (Foreign.readString <<< CX.anyToForeign) <$> matches
                    m `shouldEqual` [Right "much more"]
        
        it "should match an integer" do
            shouldCompile "I have {int} cucumbers" [CXT.int] \ex -> do
                shouldMatch ex "I have 1001 cucumbers" \matches -> do
                    let m = runExcept <$> (Foreign.readInt <<< CX.anyToForeign) <$> matches
                    m `shouldEqual` [Right 1001]
        
        it "should match a float" do
            shouldCompile "I have {float} cucumbers" [CXT.float] \ex -> do
                shouldMatch ex "I have 1001.12675 cucumbers" \matches -> do
                    let m = runExcept <$> (Foreign.readNumber <<< CX.anyToForeign) <$> matches
                    m `shouldEqual` [Right 1001.12675]
        
        it "should match a float with just a dot" do
            shouldCompile "I have {float} cucumbers" [CXT.float] \ex -> do
                shouldMatch ex "I have 1001. cucumbers" \matches -> do
                    let m = runExcept <$> (Foreign.readNumber <<< CX.anyToForeign) <$> matches
                    m `shouldEqual` [Right 1001.0]
    describe "error messages" do
        it "should provide an error message when no token can be matched" do
            case CX.compile [CXT.float, CXT.word] "I have no {bla} token" of
                Left e -> e `shouldEqual` "Could not compile cucumber expression 'I have no {bla} token':Could not find any matching expression token. Valid tokens are: float, word"
                Right _ -> fail "Compilation should have failed"
        
        it "should provide an error message when no token can be matched partially" do
            case CX.compile [CXT.float, CXT.word] "I have no {floaty} token" of
                Left e -> e `shouldEqual` "Could not compile cucumber expression 'I have no {floaty} token':Could not find any matching expression token. Valid tokens are: float, word"
                Right _ -> fail "Compilation should have failed"
