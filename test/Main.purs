module Test.Main where

import Prelude

import Effect (Effect)
import Test.CucumberExpression as CukeEx
import Test.GherkinSpec as GherkinSpec
import Test.Spec.Reporter (consoleReporter)
import Test.Spec.Runner as Spec
import Test.TagExpression as TestTagEx

main :: Effect Unit
main = Spec.run [consoleReporter] do
  GherkinSpec.spec
  TestTagEx.spec
  CukeEx.spec
