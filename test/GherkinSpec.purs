module Test.GherkinSpec where

import Prelude

import Data.Bifunctor (lmap)
import Data.Either (Either(..))
import Data.List (List(..))
import Data.List as List
import Data.List.Types (NonEmptyList(..))
import Data.Maybe (Maybe(..))
import Data.NonEmpty ((:|))
import Effect.Aff (Aff)
import Gherkin (Background(..), DataTable(..), DocString(..), Example(..), Feature(..), Step(..), StepData(..), StepType(..), Tag(..))
import Gherkin as Gherkin
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (fail, shouldEqual)
import Text.Parsing.StringParser (ParseError)


withFeature :: Either ParseError Feature -> (Feature -> Aff Unit) -> Aff Unit
withFeature (Left err) _ = fail $ "Expected feature but got a parse error: " <> show err
withFeature (Right feature) f = f feature

withExample :: Int -> Either ParseError Feature -> (Example -> Aff Unit) -> Aff Unit
withExample _ (Left err) _ = fail $ "Expected examples but got a parse error: " <> show err
withExample _ (Right (Feature {examples: Nil})) _ = fail "Expected examples in feature"
withExample idx (Right (Feature {examples})) f = 
  case List.index examples idx of
    Nothing -> fail $ "Cannot find example with index " <> show idx
    Just ex -> f ex

withStep :: Int -> Example -> (Step -> Aff Unit) -> Aff Unit
withStep _ (Example {label, steps: Nil}) _ = fail $ "Expected a step in example " <> show label
withStep idx (Example {label, steps}) f =
  case List.index steps idx of
    Nothing -> fail $ "Cannot find step " <> show idx <> " in example " <> show label 
    Just step -> f step

withBackgroundStep :: Int -> Feature -> (Step -> Aff Unit) -> Aff Unit
withBackgroundStep _ (Feature {background: Nothing}) _ = fail "Expected a step in background"
withBackgroundStep idx (Feature {background: Just (Background {steps})}) f =
  case List.index steps idx of
    Nothing -> fail $ "Cannot find step " <> show idx <> " in background"
    Just step -> f step

spec :: Spec Unit
spec = 
  describe "Gherkin parser" do
    it "should parse an empty feature" do
      let parsed = Gherkin.parse "Feature:"
      
      parsed `shouldEqual` (Right $ Feature {label: Just "", background: Nothing, tags: Nil, examples: Nil, description: Nothing})
    it "should parse a feature with a label" do
      let parsed = Gherkin.parse "Feature: Hello you there"

      parsed `shouldEqual` (Right $ Feature {label: Just "Hello you there", background: Nothing, tags: Nil, examples: Nil, description: Nothing})
    
    it "should parse a feature with whitespaces before" do
      let parsed = Gherkin.parse "  \n   \n  \r\n \r   Feature: Yes"

      parsed `shouldEqual` (Right $ Feature {label: Just "Yes", background: Nothing, tags: Nil, examples: Nil, description: Nothing})

    it "should parse a feature with whitespaces after" do
      let parsed = Gherkin.parse "Feature: Yes   \n   \n  \r\n \r   "

      parsed `shouldEqual` (Right $ Feature {label: Just "Yes", background: Nothing, tags: Nil, examples: Nil, description: Nothing})

    it "should not parse gibberish as feature" do
      let parsed = Gherkin.parse "Fateures: No"

      lmap (const "Failed") parsed `shouldEqual` (Left "Failed")

    it "should parse an empty example" do
      let parsed = Gherkin.parse "Feature: My Feature\nScenario:"
    
      withExample 0 parsed $ shouldEqual (Example {label: Just "", steps: Nil, tags: Nil , table: Nothing, description: Nothing})
        
    it "should parse an example with a label" do
      let parsed = Gherkin.parse "Feature: My Feature\nScenario: Hello you there"
    
      withExample 0 parsed $ shouldEqual (Example {label: Just "Hello you there", steps: Nil, tags: Nil , table: Nothing, description: Nothing})

    it "should parse an example with whitespace before" do
      let parsed = Gherkin.parse "Feature: My Feature\n   \n  \r\n   \r  Scenario: Hello you there"
    
      withExample 0 parsed $ shouldEqual (Example {label: Just "Hello you there", steps: Nil, tags: Nil , table: Nothing, description: Nothing})
    
    it "should parse an example with whitespace after" do
      let parsed = Gherkin.parse "Feature: My Feature\nScenario: Hello you there   \n  \r\n   \r  "
    
      withExample 0 parsed $ shouldEqual (Example {label: Just "Hello you there", steps: Nil, tags: Nil , table: Nothing, description: Nothing})
  
    it "should parse a given step" do
      let parsed = Gherkin.parse "Feature: My Feature\nScenario: My Example\nGiven something to do"

      withExample 0 parsed \example -> withStep 0 example $ shouldEqual (Step {type: Given, label: "something to do", data: NoStepData}) 
    
    it "should parse multiple given steps" do
      let parsed = Gherkin.parse "Feature: My Feature\nScenario: My Example\nGiven something to do\nGiven something else"

      withExample 0 parsed \example -> do
        withStep 0 example $ shouldEqual (Step {type: Given, label: "something to do", data: NoStepData}) 
        withStep 1 example $ shouldEqual (Step {type: Given, label: "something else", data: NoStepData}) 

    it "should parse a when step" do
      let parsed = Gherkin.parse "Feature: My Feature\nScenario: My Example\nWhen something happens"

      withExample 0 parsed \example -> do
        withStep 0 example $ shouldEqual (Step {type: When, label: "something happens", data: NoStepData}) 

    it "should parse a then step" do
      let parsed = Gherkin.parse "Feature: My Feature\nScenario: My Example\nThen do this"

      withExample 0 parsed \example -> do
        withStep 0 example $ shouldEqual (Step {type: Then, label: "do this", data: NoStepData}) 

    it "should parse an and step" do
      let parsed = Gherkin.parse "Feature: My Feature\nScenario: My Example\nAnd i'd like an ice"

      withExample 0 parsed \example -> do
        withStep 0 example $ shouldEqual (Step {type: And, label: "i'd like an ice", data: NoStepData}) 
    
    it "should parse a but step" do
      let parsed = Gherkin.parse "Feature: My Feature\nScenario: My Example\nBut with vanilla"

      withExample 0 parsed \example -> do
        withStep 0 example $ shouldEqual (Step {type: But, label: "with vanilla", data: NoStepData}) 

    it "should parse a whole set of steps with whitespaces" do
        let feature = """
          Feature: Ice

            Scenario: Cream

              Given I have 20 cents
              And I like icecream

              When I go to the shop
              Then I can buy some ice
              And be happy

          """
        let parsed = Gherkin.parse feature

        withExample 0 parsed \example -> do
          withStep 0 example $ shouldEqual (Step {type: Given, label: "I have 20 cents", data: NoStepData})  
          withStep 1 example $ shouldEqual (Step {type: And, label: "I like icecream", data: NoStepData})  
          withStep 2 example $ shouldEqual (Step {type: When, label: "I go to the shop", data: NoStepData})  
          withStep 3 example $ shouldEqual (Step {type: Then, label: "I can buy some ice", data: NoStepData})  
          withStep 4 example $ shouldEqual (Step {type: And, label: "be happy", data: NoStepData})  

    describe "datatable" do
      let header = NonEmptyList $ "test" :| List.fromFoldable ["here"]
      let row1 = NonEmptyList $ "a" :| List.fromFoldable ["b"] 
      let row2 = NonEmptyList $ "c" :| List.fromFoldable ["d"] 
      let rows = NonEmptyList $ row1 :| pure row2 
      let dataTable = DataTable header rows


      it "should parse a datable for a step" do
        let feature = """
                Feature: Ice
                  Scenario: Cream
                    Given something
                      | test | here | 
                      | a | b |   
                      | c | d | 
                    And that
            """
        let parsed = Gherkin.parse feature

        withExample 0 parsed \example -> do
            withStep 0 example $ shouldEqual (Step {type: Given, label: "something", data: StepWithDataTable dataTable})  

      it "should parse a datable for a step which is at EOF" do
        let feature = """
                Feature: Ice
                  Scenario: Cream
                    Given something
                      | test | here | 
                      | a | b |   
                      | c | d |"""
        let parsed = Gherkin.parse feature
      
        withExample 0 parsed \example -> do
            withStep 0 example $ shouldEqual (Step {type: Given, label: "something", data: StepWithDataTable dataTable})  
      

      it "should fail parsing when the table is malformed" do
        let feature = """
                Feature: Ice
                  Scenario: Cream
                    Given something
                      | test | here | 
                      | a | b   
                      | c | d |
            """
        
        let parsed = Gherkin.parse feature
      
        lmap (const "Failed") parsed `shouldEqual` (Left "Failed")

      
      it "should fail parsing when the number of columns does not match with the header" do
        let feature = """
                Feature: Ice
                  Scenario: Cream
                    Given something
                      | test | here | 
                      | a | b | c | d |
            """
        
        let parsed = Gherkin.parse feature
      
        lmap (const "Failed") parsed `shouldEqual` (Left "Failed")
    
    describe "doc string" do
      it "should parse for a step" do
        let feature = "Feature: Ice\nScenario: Cream\nGiven something\n\"\"\"\nHello there\n\"\"\""

        let parsed = Gherkin.parse feature

        withExample 0 parsed \example -> do
          withStep 0 example $ shouldEqual (Step {type: Given, label: "something", data: StepWithDocString $ DocString "Hello there"})

      it "should parse when having a follow up step" do
        let feature = "Feature: Ice\nScenario: Cream\nGiven something\n\"\"\"\nHello there\n\"\"\"\nAnd that"

        let parsed = Gherkin.parse feature

        withExample 0 parsed \example -> do
          withStep 1 example $ shouldEqual (Step {type: And, label: "that", data: NoStepData})

      it "should parse with indent" do
        let feature = "Feature: Ice\nScenario: Cream\nGiven something\n   \"\"\"\n   Hello there\n   \"\"\""

        let parsed = Gherkin.parse feature

        withExample 0 parsed \example -> do
          withStep 0 example $ shouldEqual (Step {type: Given, label: "something", data: StepWithDocString $ DocString "Hello there"})

      it "should parse multiple lines" do
        let feature = "Feature: Ice\nScenario: Cream\nGiven something\n   \"\"\"\n   Hello there\n   this is\n   doc string\n   \"\"\""

        let parsed = Gherkin.parse feature

        withExample 0 parsed \example -> do
          withStep 0 example $ shouldEqual (Step {type: Given, label: "something", data: StepWithDocString $ DocString "Hello there\nthis is\ndoc string"})

      it "should not parse with wrong indention" do
        let feature = "Feature: Ice\nScenario: Cream\nGiven something\n   \"\"\"\n   Hello there\n         too much\n  \"\"\""

        let parsed = Gherkin.parse feature

        lmap (const "Failed") parsed `shouldEqual` (Left "Failed")

    describe "tags" do
      it "should parse tags on a feature" do
        let feature = """
            @foo @bar
            Feature: Tagged
        """

        let parsed = Gherkin.parse feature

        parsed `shouldEqual` (Right $ Feature {label: Just "Tagged", background: Nothing, examples: Nil, tags: List.fromFoldable [Tag "foo", Tag "bar"], description: Nothing})

      it "should parse tags on an example" do
        let feature = """
            Feature: Tagged
              @foo @bar
              Scenario: Tagged
        """

        let parsed = Gherkin.parse feature

        withExample 0 parsed $ shouldEqual (Example {label: Just "Tagged", steps: Nil , tags: List.fromFoldable [Tag "foo", Tag "bar"], table: Nothing, description: Nothing})
      
      it "should parse tags on the second example" do
        let feature = """
            Feature: whatever
            Scenario: No tag
            Given this

            @foo
            Scenario: Tag
            Given that
          """
          
        let parsed = Gherkin.parse feature

        withExample 1 parsed \(Example {tags}) ->
          tags `shouldEqual`  List.singleton (Tag "foo")

    describe "outline" do

      let header = NonEmptyList $ "test" :| List.fromFoldable ["here"]
      let row1 = NonEmptyList $ "a" :| List.fromFoldable ["b"] 
      let row2 = NonEmptyList $ "c" :| List.fromFoldable ["d"] 
      let rows = NonEmptyList $ row1 :| pure row2 
      let dataTable = DataTable header rows

      it "should provide the examples datatable" do
        let feature = """
            Feature: Tagged
              Scenario: something
                Given foo
                Then bar
              
              Scenario Outline: Tagged
                Given foo
                Then bar
            
                Examples:
                  | test | here |
                  | a | b |
                  | c | d |
        """

        let parsed = Gherkin.parse feature

        withExample 1 parsed \(Example {table}) -> do
          table `shouldEqual` (Just dataTable)
    
    describe "background" do
      it "should parse a background with a label" do
        let feature = """
            Feature: With Background
              Background: Background label
                Given I am here
        """
        
        let parsed = Gherkin.parse feature

        withFeature parsed \(Feature {background }) -> do
          case background of
            Nothing -> fail "Expected a background"
            Just (Background {label}) -> label `shouldEqual` (Just "Background label")

      it "should parse the background of a feature" do
        let feature = """
            Feature: With Background
              Background:
                Given I am here
                Then I do this
              
              Scenario: Test
                Given Whatever
              
              Scenario: Test2
                Given Whatever
        """
        
        let parsed = Gherkin.parse feature

        withFeature parsed \feat -> do
          withBackgroundStep 0 feat \step -> 
            step `shouldEqual` (Step {type: Given, label: "I am here", data: NoStepData})
        
        withFeature parsed \feat -> do
          withBackgroundStep 1 feat \step -> 
            step `shouldEqual` (Step {type: Then, label: "I do this", data: NoStepData})
    
    describe "comments" do
      it "should ignore comments" do
        let feature = """
            # a comment
            Feature: Test # this is a test

              #comment again
              Scenario: test again
          """
        
        let parsed = Gherkin.parse feature

        withFeature parsed \(Feature {label}) -> do
          label `shouldEqual` (Just "Test")
        
        withExample 0 parsed \(Example {label}) -> do
          label `shouldEqual` (Just "test again")

    describe "feature description" do
      it "should parse the description of a feature at eof" do
          let feature = """
              Feature: Test
              This is the description
              of the feature
              """
          
          let parsed = Gherkin.parse feature

          withFeature parsed \(Feature {description}) -> do
            description `shouldEqual` (Just "This is the description\nof the feature")
      
      it "should parse the description of a feature till the beginning of a background" do
          let feature = """
              Feature: Test
              This is the description
              of the feature
            
              Background:
              Given this
              """
          
          let parsed = Gherkin.parse feature

          withFeature parsed \(Feature {description}) -> do
            description `shouldEqual` (Just "This is the description\nof the feature")
      
      it "should parse the description of a feature till the beginning of a scenario" do
          let feature = """
              Feature: Test
              This is the description
              of the feature
            
              Scenario:
              Given this
              """
          
          let parsed = Gherkin.parse feature

          withFeature parsed \(Feature {description}) -> do
            description `shouldEqual` (Just "This is the description\nof the feature")
      
      it "should parse the description of a feature till the beginning of a scenario with tags" do
          let feature = """
              Feature: Test
              This is the description
              of the feature
            
              @foo
              Scenario:
              Given this
              """
          
          let parsed = Gherkin.parse feature

          withFeature parsed \(Feature {description}) -> do
            description `shouldEqual` (Just "This is the description\nof the feature")

      it "should parse the description of an example till a step" do
          let feature = """
              Feature: Test

              Scenario:
              Some random text
              is placed here

              Given this
              """
          
          let parsed = Gherkin.parse feature

          withExample 0 parsed \(Example {description, steps}) -> do
            description `shouldEqual` (Just "Some random text\nis placed here")
            List.length steps `shouldEqual` 1
      
      it "should parse the description of the background" do
        let feature = """
            Feature: Test

            Background:
            Some random text
            is placed here

            Given this
            """
          
        let parsed = Gherkin.parse feature

        withFeature parsed $ case _ of
          Feature {background: Nothing} -> fail "Expected background to have been parsed"
          Feature {background: Just (Background {description, steps})} -> do
            description `shouldEqual` (Just "Some random text\nis placed here")
            List.length steps `shouldEqual` 1
