module Test.TagExpression where

import Prelude

import Data.Either (Either(..))
import Data.Foldable (class Foldable)
import Effect.Aff (Aff)
import Gherkin (Tag(..))
import Gherkin.Expression.TagExpression as TX
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (fail, shouldEqual)

compileAndEval :: ∀ f. Foldable f => String -> f Tag -> (Boolean -> Aff Unit) -> Aff Unit
compileAndEval str tags f = do
    case TX.compile str of
        Left e -> fail $ "Failed to compile with: " <> e
        Right ex -> f $ TX.eval ex tags

spec :: Spec Unit
spec = describe "Tag expression" do
    it "should eval a single tag as expression" do
        compileAndEval "@tag" [Tag "tag"] (_ `shouldEqual` true)
    it "should eval an 'and'-expression" do
        let expr = "@tag and @other"
        compileAndEval expr [Tag "tag"] (_ `shouldEqual` false)
        compileAndEval expr [Tag "tag", Tag "other"] (_ `shouldEqual` true)
    it "should eval an 'or'-expression" do
        let expr = "@tag or @other"
        compileAndEval expr [Tag "tag"] (_ `shouldEqual` true)
        compileAndEval expr [Tag "other"] (_ `shouldEqual` true)
    it "should eval a not" do
        let expr = "not @tag"
        compileAndEval expr [Tag "other"] (_ `shouldEqual` true)
        compileAndEval expr [Tag "tag"] (_ `shouldEqual` false)
    it "should eval 'and', 'or' and 'not'" do
        let expr = "@tag and not @other or @foo"
        compileAndEval expr [Tag "tag"] (_ `shouldEqual` true)
        compileAndEval expr [Tag "other"] (_ `shouldEqual` false)
        compileAndEval expr [Tag "foo", Tag "tag"] (_ `shouldEqual` true)
    it "should not compile a bad expression" do
        case TX.compile "@tag and or @bad" of
            Right _ -> fail "Expected compile to fail"
            Left _ -> pure unit
    it "should compile with brackets" do
        compileAndEval "(@tag)" [Tag "tag"] (_ `shouldEqual` true)
    it "should compile a complex expression" do
        let expr = "@tag1 and not (@tag2 or @tag3)"
        compileAndEval expr [Tag "tag1"] (_ `shouldEqual` true)
        compileAndEval expr [Tag "tag2"] (_ `shouldEqual` false)
        compileAndEval expr [Tag "tag3", Tag "tag1"] (_ `shouldEqual` false)
        compileAndEval expr [Tag "tag4", Tag "tag1"] (_ `shouldEqual` true)
    it "should not compile an empty input" do
        case TX.compile "" of
            Right _ -> fail "Expected compile to fail"
            Left _ -> pure unit
