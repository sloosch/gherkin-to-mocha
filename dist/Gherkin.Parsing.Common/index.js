// Generated by purs version 0.12.0
"use strict";
var Control_Alt = require("../Control.Alt/index.js");
var Control_Applicative = require("../Control.Applicative/index.js");
var Control_Apply = require("../Control.Apply/index.js");
var Control_Bind = require("../Control.Bind/index.js");
var Data_Foldable = require("../Data.Foldable/index.js");
var Data_Function = require("../Data.Function/index.js");
var Data_Functor = require("../Data.Functor/index.js");
var Data_List_Types = require("../Data.List.Types/index.js");
var Data_Monoid = require("../Data.Monoid/index.js");
var Data_Ord = require("../Data.Ord/index.js");
var Data_Semigroup = require("../Data.Semigroup/index.js");
var Data_Show = require("../Data.Show/index.js");
var Data_String = require("../Data.String/index.js");
var Data_String_CodePoints = require("../Data.String.CodePoints/index.js");
var Data_String_CodeUnits = require("../Data.String.CodeUnits/index.js");
var Data_Unit = require("../Data.Unit/index.js");
var Prelude = require("../Prelude/index.js");
var Text_Parsing_StringParser = require("../Text.Parsing.StringParser/index.js");
var Text_Parsing_StringParser_Combinators = require("../Text.Parsing.StringParser.Combinators/index.js");
var Text_Parsing_StringParser_String = require("../Text.Parsing.StringParser.String/index.js");
var Escaped = (function () {
    function Escaped(value0) {
        this.value0 = value0;
    };
    Escaped.create = function (value0) {
        return new Escaped(value0);
    };
    return Escaped;
})();
var Raw = (function () {
    function Raw(value0) {
        this.value0 = value0;
    };
    Raw.create = function (value0) {
        return new Raw(value0);
    };
    return Raw;
})();
var $$unescape = function (v) {
    if (v instanceof Escaped) {
        return v.value0;
    };
    if (v instanceof Raw) {
        return v.value0;
    };
    throw new Error("Failed pattern match at Gherkin.Parsing.Common line 44, column 1 - line 44, column 28: " + [ v.constructor.name ]);
};
var stringTillP = function (end) {
    return Data_Functor.map(Text_Parsing_StringParser.functorParser)(Data_Foldable.foldMap(Data_List_Types.foldableList)(Data_Monoid.monoidString)(Data_String_CodeUnits.singleton))(Text_Parsing_StringParser_Combinators.manyTill(Text_Parsing_StringParser_String.anyChar)(end));
};
var newlineP = Data_Functor["void"](Text_Parsing_StringParser.functorParser)(Text_Parsing_StringParser_Combinators.withError(Text_Parsing_StringParser_String.regex("\\r?\\n"))("Expected newline"));
var stringTillNewlineP = stringTillP(newlineP);
var indentP = Data_Functor.map(Text_Parsing_StringParser.functorParser)(Data_String_CodePoints.length)(Text_Parsing_StringParser_String.regex("[ \x09]*"));
var expectIndentP = function (len) {
    return Control_Bind.bind(Text_Parsing_StringParser.bindParser)(indentP)(function (v) {
        var $7 = v < len;
        if ($7) {
            return Text_Parsing_StringParser.fail("Expected indent of " + (Data_Show.show(Data_Show.showInt)(len) + (" but got " + Data_Show.show(Data_Show.showInt)(v))));
        };
        return Control_Applicative.pure(Text_Parsing_StringParser.applicativeParser)(Data_Unit.unit);
    });
};
var escapedP = function (s) {
    return Control_Alt.alt(Text_Parsing_StringParser.altParser)(Data_Functor.map(Text_Parsing_StringParser.functorParser)(Escaped.create)(Control_Apply.applySecond(Text_Parsing_StringParser.applyParser)(Text_Parsing_StringParser_String["char"]("\\"))(s)))(Data_Functor.map(Text_Parsing_StringParser.functorParser)(Raw.create)(s));
};
var onlyEscapedP = function (s) {
    return Text_Parsing_StringParser["try"](Control_Bind.bind(Text_Parsing_StringParser.bindParser)(escapedP(s))(function (v) {
        if (v instanceof Escaped) {
            return Control_Applicative.pure(Text_Parsing_StringParser.applicativeParser)(v.value0);
        };
        if (v instanceof Raw) {
            return Text_Parsing_StringParser.fail("Expected '" + (Data_Show.show(Data_Show.showChar)(v.value0) + "' to be escaped"));
        };
        throw new Error("Failed pattern match at Gherkin.Parsing.Common line 52, column 42 - line 54, column 50: " + [ v.constructor.name ]);
    }));
};
var eolP = Control_Alt.alt(Text_Parsing_StringParser.altParser)(Text_Parsing_StringParser_String.eof)(newlineP);
var stringTillEolP = stringTillP(eolP);
module.exports = {
    newlineP: newlineP,
    eolP: eolP,
    indentP: indentP,
    expectIndentP: expectIndentP,
    stringTillEolP: stringTillEolP,
    stringTillNewlineP: stringTillNewlineP,
    stringTillP: stringTillP,
    Escaped: Escaped,
    Raw: Raw,
    "unescape": $$unescape,
    escapedP: escapedP,
    onlyEscapedP: onlyEscapedP
};
