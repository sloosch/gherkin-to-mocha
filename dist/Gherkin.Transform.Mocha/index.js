// Generated by purs version 0.12.0
"use strict";
var $foreign = require("./foreign.js");
var Control_Applicative = require("../Control.Applicative/index.js");
var Control_Apply = require("../Control.Apply/index.js");
var Control_Bind = require("../Control.Bind/index.js");
var Control_Monad_Reader = require("../Control.Monad.Reader/index.js");
var Control_Monad_Reader_Class = require("../Control.Monad.Reader.Class/index.js");
var Control_Monad_Reader_Trans = require("../Control.Monad.Reader.Trans/index.js");
var Control_Monad_Writer = require("../Control.Monad.Writer/index.js");
var Control_Monad_Writer_Class = require("../Control.Monad.Writer.Class/index.js");
var Control_Monad_Writer_Trans = require("../Control.Monad.Writer.Trans/index.js");
var Control_Semigroupoid = require("../Control.Semigroupoid/index.js");
var Data_Array = require("../Data.Array/index.js");
var Data_Foldable = require("../Data.Foldable/index.js");
var Data_FoldableWithIndex = require("../Data.FoldableWithIndex/index.js");
var Data_Function = require("../Data.Function/index.js");
var Data_Functor = require("../Data.Functor/index.js");
var Data_Identity = require("../Data.Identity/index.js");
var Data_List = require("../Data.List/index.js");
var Data_List_NonEmpty = require("../Data.List.NonEmpty/index.js");
var Data_List_Types = require("../Data.List.Types/index.js");
var Data_Maybe = require("../Data.Maybe/index.js");
var Data_Monoid = require("../Data.Monoid/index.js");
var Data_Newtype = require("../Data.Newtype/index.js");
var Data_Semigroup = require("../Data.Semigroup/index.js");
var Data_Semigroup_Foldable = require("../Data.Semigroup.Foldable/index.js");
var Data_Show = require("../Data.Show/index.js");
var Data_String = require("../Data.String/index.js");
var Data_String_Common = require("../Data.String.Common/index.js");
var Data_String_Pattern = require("../Data.String.Pattern/index.js");
var Data_Unit = require("../Data.Unit/index.js");
var Gherkin = require("../Gherkin/index.js");
var Gherkin_Expression_TagExpression = require("../Gherkin.Expression.TagExpression/index.js");
var Gherkin_Transform_Types = require("../Gherkin.Transform.Types/index.js");
var Prelude = require("../Prelude/index.js");
var Mocha = function (x) {
    return x;
};
var Describe = (function () {
    function Describe() {

    };
    Describe.value = new Describe();
    return Describe;
})();
var DescribeSkip = (function () {
    function DescribeSkip() {

    };
    DescribeSkip.value = new DescribeSkip();
    return DescribeSkip;
})();
var DescribeOnly = (function () {
    function DescribeOnly() {

    };
    DescribeOnly.value = new DescribeOnly();
    return DescribeOnly;
})();
var toJsArray = function (dictFoldable) {
    return function (dictFunctor) {
        return function (cells) {
            return "[" + (Data_Foldable.intercalate(dictFoldable)(Data_Monoid.monoidString)(", ")(Data_Functor.map(dictFunctor)($foreign.stringifyString)(cells)) + "]");
        };
    };
};
var stepDataToJs = function (v) {
    if (v instanceof Gherkin.NoStepData) {
        return "null";
    };
    if (v instanceof Gherkin.StepWithDocString) {
        return $foreign.stringifyString(v.value0);
    };
    if (v instanceof Gherkin.StepWithDataTable) {
        return Data_Foldable.intercalate(Data_Foldable.foldableArray)(Data_Monoid.monoidString)("\x0a")([ "{", "headers: " + (toJsArray(Data_List_Types.foldableNonEmptyList)(Data_List_Types.functorNonEmptyList)(v.value0.value0) + ","), "rows: [" + (Data_Semigroup_Foldable.intercalateMap(Data_List_Types.foldable1NonEmptyList)(Data_Semigroup.semigroupString)(",\x0a")(toJsArray(Data_List_Types.foldableNonEmptyList)(Data_List_Types.functorNonEmptyList))(v.value0.value1) + "]"), "}" ]);
    };
    throw new Error("Failed pattern match at Gherkin.Transform.Mocha line 49, column 1 - line 49, column 31: " + [ v.constructor.name ]);
};
var stepSupportCode = function (dictMonadTell) {
    return function (v) {
        var stepDataJs = stepDataToJs(v.data);
        var label = $foreign.stringifyString(v.label);
        if (v.type instanceof Gherkin.Given) {
            return Control_Monad_Writer_Class.tell(dictMonadTell)([ "gherkinSupport.executeGiven(" + (label + (", " + (stepDataJs + ");"))) ]);
        };
        if (v.type instanceof Gherkin.When) {
            return Control_Monad_Writer_Class.tell(dictMonadTell)([ "gherkinSupport.executeWhen(" + (label + (", " + (stepDataJs + ");"))) ]);
        };
        if (v.type instanceof Gherkin.Then) {
            return Control_Monad_Writer_Class.tell(dictMonadTell)([ "gherkinSupport.executeThen(" + (label + (", " + (stepDataJs + ");"))) ]);
        };
        if (v.type instanceof Gherkin.And) {
            return Control_Monad_Writer_Class.tell(dictMonadTell)([ "gherkinSupport.executeAnd(" + (label + (", " + (stepDataJs + ");"))) ]);
        };
        if (v.type instanceof Gherkin.But) {
            return Control_Monad_Writer_Class.tell(dictMonadTell)([ "gherkinSupport.executeBut(" + (label + (", " + (stepDataJs + ");"))) ]);
        };
        throw new Error("Failed pattern match at Gherkin.Transform.Mocha line 114, column 5 - line 119, column 90: " + [ v.type.constructor.name ]);
    };
};
var itStep = function (dictMonadTell) {
    return function (v) {
        var label = $foreign.stringifyString(Data_Show.show(Gherkin.showStepType)(v.type) + (" " + v.label));
        return Control_Bind.discard(Control_Bind.discardUnit)((dictMonadTell.Monad0()).Bind1())(Control_Monad_Writer_Class.tell(dictMonadTell)([ "it(" + (label + ", () => {") ]))(function () {
            return Control_Bind.discard(Control_Bind.discardUnit)((dictMonadTell.Monad0()).Bind1())(stepSupportCode(dictMonadTell)(v))(function () {
                return Control_Monad_Writer_Class.tell(dictMonadTell)([ "});" ]);
            });
        });
    };
};
var import$prime = function (dictMonadTell) {
    return function (i) {
        return Control_Monad_Writer_Class.tell(dictMonadTell)([ "import " + ($foreign.stringifyString(i) + ";") ]);
    };
};
var describeTypeToJs = function (v) {
    if (v instanceof Describe) {
        return "describe";
    };
    if (v instanceof DescribeSkip) {
        return "describe.skip";
    };
    if (v instanceof DescribeOnly) {
        return "describe.only";
    };
    throw new Error("Failed pattern match at Gherkin.Transform.Mocha line 41, column 1 - line 41, column 39: " + [ v.constructor.name ]);
};
var describeTypeFromTags = function (dictMonadAsk) {
    return function (tags) {
        return Control_Bind.bind((dictMonadAsk.Monad0()).Bind1())(Control_Monad_Reader_Class.asks(dictMonadAsk)(Data_Newtype.unwrap(Gherkin_Transform_Types.newtypeTransformConfiguration)))(function (v) {
            return Control_Applicative.pure((dictMonadAsk.Monad0()).Applicative0())((function () {
                if (v.skipTagExpression instanceof Data_Maybe.Just && Gherkin_Expression_TagExpression["eval"](Data_List_Types.foldableList)(v.skipTagExpression.value0)(tags)) {
                    return DescribeSkip.value;
                };
                if (v.onlyTagExpression instanceof Data_Maybe.Just && Gherkin_Expression_TagExpression["eval"](Data_List_Types.foldableList)(v.onlyTagExpression.value0)(tags)) {
                    return DescribeOnly.value;
                };
                return Describe.value;
            })());
        });
    };
};
var forScenarioData = function (dictMonadAsk) {
    return function (featTags) {
        return function (f) {
            return function (v) {
                var exLabel = Data_Maybe.fromMaybe("Scenario: ")(v.label);
                var changeStepLabel = function (fm) {
                    return function (v1) {
                        return {
                            type: v1.type,
                            label: fm(v1.label),
                            data: v1.data
                        };
                    };
                };
                var uniqTags = Data_List.nub(Gherkin.eqTag)(Data_Semigroup.append(Data_List_Types.semigroupList)(featTags)(v.tags));
                return Control_Bind.bind((dictMonadAsk.Monad0()).Bind1())(describeTypeFromTags(dictMonadAsk)(v.tags))(function (v1) {
                    if (v.table instanceof Data_Maybe.Nothing) {
                        return f(exLabel)(v1)(v.steps)(uniqTags);
                    };
                    if (v.table instanceof Data_Maybe.Just) {
                        var headerArr = Data_Array.fromFoldable(Data_List_Types.foldableNonEmptyList)(v.table.value0.value0);
                        return Data_Foldable.for_((dictMonadAsk.Monad0()).Applicative0())(Data_List_Types.foldableNonEmptyList)(v.table.value0.value1)(function (row) {
                            var replaceWithCell = function (idx) {
                                return function (header) {
                                    return Data_String_Common.replaceAll(Data_String_Pattern.Pattern("<" + (header + ">")))(Data_String_Pattern.Replacement(Data_Maybe.fromMaybe("")(Data_List_NonEmpty.index(row)(idx))));
                                };
                            };
                            var newLabel = Data_FoldableWithIndex.foldrWithIndex(Data_FoldableWithIndex.foldableWithIndexArray)(replaceWithCell)(exLabel)(headerArr);
                            var newSteps = Data_FoldableWithIndex.foldrWithIndex(Data_FoldableWithIndex.foldableWithIndexArray)(function (idx) {
                                return function (header) {
                                    return Data_Functor.map(Data_List_Types.functorList)(changeStepLabel(replaceWithCell(idx)(header)));
                                };
                            })(v.steps)(headerArr);
                            return f(newLabel)(v1)(newSteps)(uniqTags);
                        });
                    };
                    throw new Error("Failed pattern match at Gherkin.Transform.Mocha line 134, column 5 - line 146, column 58: " + [ v.table.constructor.name ]);
                });
            };
        };
    };
};
var describe = function (dictMonadTell) {
    return function (describeType) {
        return function (label) {
            return function (m) {
                return Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Monad_Writer_Class.tell(dictMonadTell)([ describeTypeToJs(describeType) + ("(" + ($foreign.stringifyString(label) + ", () => {")) ]))(m))(Control_Monad_Writer_Class.tell(dictMonadTell)([ "});" ]));
            };
        };
    };
};
var beforeEach = function (dictMonadTell) {
    return function (m) {
        return Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Monad_Writer_Class.tell(dictMonadTell)([ "beforeEach(() => {" ]))(m))(Control_Monad_Writer_Class.tell(dictMonadTell)([ "});" ]));
    };
};
var before = function (dictMonadTell) {
    return function (m) {
        return Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Monad_Writer_Class.tell(dictMonadTell)([ "before(() => {" ]))(m))(Control_Monad_Writer_Class.tell(dictMonadTell)([ "});" ]));
    };
};
var afterEach = function (dictMonadTell) {
    return function (m) {
        return Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Monad_Writer_Class.tell(dictMonadTell)([ "afterEach(() => {" ]))(m))(Control_Monad_Writer_Class.tell(dictMonadTell)([ "});" ]));
    };
};
var after = function (dictMonadTell) {
    return function (m) {
        return Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Apply.applySecond(((dictMonadTell.Monad0()).Bind1()).Apply0())(Control_Monad_Writer_Class.tell(dictMonadTell)([ "after(() => {" ]))(m))(Control_Monad_Writer_Class.tell(dictMonadTell)([ "});" ]));
    };
};
var scenarioCode = function (dictMonadTell) {
    return function (background) {
        return function (exLabel) {
            return function (describeType) {
                return function (steps) {
                    return function (tags) {
                        return describe(dictMonadTell)(describeType)(exLabel)((function () {
                            var tagsAsJsArray = toJsArray(Data_List_Types.foldableList)(Data_List_Types.functorList)(Data_Functor.map(Data_List_Types.functorList)(Data_Newtype.unwrap(Gherkin.newTypeTag))(tags));
                            return Control_Bind.discard(Control_Bind.discardUnit)((dictMonadTell.Monad0()).Bind1())(before(dictMonadTell)(Control_Monad_Writer_Class.tell(dictMonadTell)([ "gherkinSupport.excuteHookBeforeExample(" + (tagsAsJsArray + ");") ])))(function () {
                                return Control_Bind.discard(Control_Bind.discardUnit)((dictMonadTell.Monad0()).Bind1())(after(dictMonadTell)(Control_Monad_Writer_Class.tell(dictMonadTell)([ "gherkinSupport.excuteHookAfterExample(" + (tagsAsJsArray + ");") ])))(function () {
                                    return Control_Bind.discard(Control_Bind.discardUnit)((dictMonadTell.Monad0()).Bind1())(beforeEach(dictMonadTell)(Control_Monad_Writer_Class.tell(dictMonadTell)([ "gherkinSupport.excuteHookBeforeStep(" + (tagsAsJsArray + ");") ])))(function () {
                                        return Control_Bind.discard(Control_Bind.discardUnit)((dictMonadTell.Monad0()).Bind1())(afterEach(dictMonadTell)(Control_Monad_Writer_Class.tell(dictMonadTell)([ "gherkinSupport.excuteHookAfterStep(" + (tagsAsJsArray + ");") ])))(function () {
                                            return Control_Bind.discard(Control_Bind.discardUnit)((dictMonadTell.Monad0()).Bind1())((function () {
                                                if (background instanceof Data_Maybe.Just) {
                                                    return before(dictMonadTell)(Data_Foldable.for_((dictMonadTell.Monad0()).Applicative0())(Data_List_Types.foldableList)(background.value0.steps)(stepSupportCode(dictMonadTell)));
                                                };
                                                if (background instanceof Data_Maybe.Nothing) {
                                                    return Control_Applicative.pure((dictMonadTell.Monad0()).Applicative0())(Data_Unit.unit);
                                                };
                                                throw new Error("Failed pattern match at Gherkin.Transform.Mocha line 104, column 9 - line 106, column 33: " + [ background.constructor.name ]);
                                            })())(function () {
                                                return Data_Foldable.for_((dictMonadTell.Monad0()).Applicative0())(Data_List_Types.foldableList)(steps)(itStep(dictMonadTell));
                                            });
                                        });
                                    });
                                });
                            });
                        })());
                    };
                };
            };
        };
    };
};
var transform = function (config) {
    return function (v) {
        return Mocha(Data_Foldable.intercalate(Data_Foldable.foldableArray)(Data_Monoid.monoidString)("\x0a")(Control_Monad_Writer.execWriter(Data_Function.flip(Control_Monad_Reader_Trans.runReaderT)(config)(Control_Bind.bind(Control_Monad_Reader_Trans.bindReaderT(Control_Monad_Writer_Trans.bindWriterT(Data_Semigroup.semigroupArray)(Data_Identity.bindIdentity)))(Control_Monad_Reader_Class.asks(Control_Monad_Reader_Trans.monadAskReaderT(Control_Monad_Writer_Trans.monadWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))(function ($57) {
            return (function (v1) {
                return v1.pathToStepDefintions;
            })(Data_Newtype.unwrap(Gherkin_Transform_Types.newtypeTransformConfiguration)($57));
        }))(function (v1) {
            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Reader_Trans.bindReaderT(Control_Monad_Writer_Trans.bindWriterT(Data_Semigroup.semigroupArray)(Data_Identity.bindIdentity)))(import$prime(Control_Monad_Reader_Trans.monadTellReaderT(Control_Monad_Writer_Trans.monadTellWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))(v1))(function () {
                return Control_Bind.bind(Control_Monad_Reader_Trans.bindReaderT(Control_Monad_Writer_Trans.bindWriterT(Data_Semigroup.semigroupArray)(Data_Identity.bindIdentity)))(describeTypeFromTags(Control_Monad_Reader_Trans.monadAskReaderT(Control_Monad_Writer_Trans.monadWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))(v.tags))(function (v2) {
                    var label = Data_Maybe.fromMaybe("Feature:")(v.label);
                    return describe(Control_Monad_Reader_Trans.monadTellReaderT(Control_Monad_Writer_Trans.monadTellWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))(v2)(label)((function () {
                        var tagsAsJsArray = toJsArray(Data_List_Types.foldableList)(Data_List_Types.functorList)(Data_Functor.map(Data_List_Types.functorList)(Data_Newtype.unwrap(Gherkin.newTypeTag))(v.tags));
                        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Reader_Trans.bindReaderT(Control_Monad_Writer_Trans.bindWriterT(Data_Semigroup.semigroupArray)(Data_Identity.bindIdentity)))(before(Control_Monad_Reader_Trans.monadTellReaderT(Control_Monad_Writer_Trans.monadTellWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))(Control_Monad_Writer_Class.tell(Control_Monad_Reader_Trans.monadTellReaderT(Control_Monad_Writer_Trans.monadTellWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))([ "gherkinSupport.excuteHookBeforeFeature(" + (tagsAsJsArray + ");") ])))(function () {
                            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Reader_Trans.bindReaderT(Control_Monad_Writer_Trans.bindWriterT(Data_Semigroup.semigroupArray)(Data_Identity.bindIdentity)))(after(Control_Monad_Reader_Trans.monadTellReaderT(Control_Monad_Writer_Trans.monadTellWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))(Control_Monad_Writer_Class.tell(Control_Monad_Reader_Trans.monadTellReaderT(Control_Monad_Writer_Trans.monadTellWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))([ "gherkinSupport.excuteHookAfterFeature(" + (tagsAsJsArray + ");") ])))(function () {
                                return Data_Foldable.for_(Control_Monad_Reader_Trans.applicativeReaderT(Control_Monad_Writer_Trans.applicativeWriterT(Data_Monoid.monoidArray)(Data_Identity.applicativeIdentity)))(Data_List_Types.foldableList)(v.examples)(forScenarioData(Control_Monad_Reader_Trans.monadAskReaderT(Control_Monad_Writer_Trans.monadWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))(v.tags)(scenarioCode(Control_Monad_Reader_Trans.monadTellReaderT(Control_Monad_Writer_Trans.monadTellWriterT(Data_Monoid.monoidArray)(Data_Identity.monadIdentity)))(v.background)));
                            });
                        });
                    })());
                });
            });
        })))));
    };
};
var transformToMocha = new Gherkin_Transform_Types.TransformTo(transform);
module.exports = {
    transformToMocha: transformToMocha
};
