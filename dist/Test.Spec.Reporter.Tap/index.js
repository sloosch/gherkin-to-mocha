// Generated by purs version 0.12.0
"use strict";
var Control_Applicative = require("../Control.Applicative/index.js");
var Control_Bind = require("../Control.Bind/index.js");
var Data_Either = require("../Data.Either/index.js");
var Data_Function = require("../Data.Function/index.js");
var Data_Functor = require("../Data.Functor/index.js");
var Data_Maybe = require("../Data.Maybe/index.js");
var Data_Semigroup = require("../Data.Semigroup/index.js");
var Data_Semiring = require("../Data.Semiring/index.js");
var Data_Show = require("../Data.Show/index.js");
var Data_String = require("../Data.String/index.js");
var Data_String_Common = require("../Data.String.Common/index.js");
var Data_String_Pattern = require("../Data.String.Pattern/index.js");
var Data_String_Regex = require("../Data.String.Regex/index.js");
var Data_Unit = require("../Data.Unit/index.js");
var Effect = require("../Effect/index.js");
var Effect_Console = require("../Effect.Console/index.js");
var Partial_Unsafe = require("../Partial.Unsafe/index.js");
var Prelude = require("../Prelude/index.js");
var Test_Spec_Reporter_Base = require("../Test.Spec.Reporter.Base/index.js");
var Test_Spec_Runner = require("../Test.Spec.Runner/index.js");
var Test_Spec_Runner_Event = require("../Test.Spec.Runner.Event/index.js");
var Test_Spec_Summary = require("../Test.Spec.Summary/index.js");
var escTitle = (function () {
    var rex = Data_Either.fromRight()(Data_String_Regex.regex("#")(Data_String_Regex.parseFlags("g")));
    return Data_String_Regex.replace(rex)("");
})();
var escMsg = (function () {
    var rex = Data_Either.fromRight()(Data_String_Regex.regex("^")(Data_String_Regex.parseFlags("gm")));
    return Data_String_Regex.replace(rex)("  ");
})();
var tapReporter = (function () {
    var update = function (n) {
        return function (v) {
            if (v instanceof Test_Spec_Runner_Event.Start) {
                return Data_Functor.voidRight(Effect.functorEffect)(n)(Effect_Console.log("1.." + Data_Show.show(Data_Show.showInt)(v.value0)));
            };
            if (v instanceof Test_Spec_Runner_Event.TestEnd) {
                return Control_Applicative.pure(Effect.applicativeEffect)(n + 1 | 0);
            };
            if (v instanceof Test_Spec_Runner_Event.Pending) {
                return Data_Functor.voidRight(Effect.functorEffect)(n)(Effect_Console.log("ok " + (Data_Show.show(Data_Show.showInt)(n) + (" " + (escTitle(v.value0) + " # SKIP -")))));
            };
            if (v instanceof Test_Spec_Runner_Event.Pass) {
                return Data_Functor.voidRight(Effect.functorEffect)(n)(Effect_Console.log("ok " + (Data_Show.show(Data_Show.showInt)(n) + (" " + escTitle(v.value0)))));
            };
            if (v instanceof Test_Spec_Runner_Event.Fail) {
                return Data_Functor.voidRight(Effect.functorEffect)(n)(function __do() {
                    Effect_Console.log("not ok " + (Data_Show.show(Data_Show.showInt)(n) + (" " + escTitle(v.value0))))();
                    Effect_Console.log(escMsg(v.value1))();
                    if (v.value2 instanceof Data_Maybe.Nothing) {
                        return Data_Unit.unit;
                    };
                    if (v.value2 instanceof Data_Maybe.Just) {
                        return Effect_Console.log(Data_String_Common.joinWith("\x0a")(Data_Functor.map(Data_Functor.functorArray)(Data_Semigroup.append(Data_Semigroup.semigroupString)("    "))(Data_String_Common.split("\x0a")(v.value2.value0))))();
                    };
                    throw new Error("Failed pattern match at Test.Spec.Reporter.Tap line 33, column 7 - line 35, column 82: " + [ v.value2.constructor.name ]);
                });
            };
            if (v instanceof Test_Spec_Runner_Event.End) {
                return function __do() {
                    (function () {
                        var v1 = Test_Spec_Summary.summarize(v.value0);
                        Effect_Console.log("# tests " + Data_Show.show(Data_Show.showInt)((v1.value1 + v1.value0 | 0) + v1.value2 | 0))();
                        Effect_Console.log("# pass " + Data_Show.show(Data_Show.showInt)(v1.value0 + v1.value2 | 0))();
                        return Effect_Console.log("# fail " + Data_Show.show(Data_Show.showInt)(v1.value1))();
                    })();
                    return n;
                };
            };
            return Control_Applicative.pure(Effect.applicativeEffect)(n);
        };
    };
    return Test_Spec_Reporter_Base.defaultReporter(1)(update);
})();
module.exports = {
    tapReporter: tapReporter
};
