import * as support from './dist/Gherkin.Support';
import { Just } from './dist/Data.Maybe';
import { Nothing } from './dist/Data.Maybe';
import { isLeft, fromLeft } from './dist/Data.Either';
import { RegisterStepTestExpression, RegisterStepTestRegex } from './dist/Gherkin.Support';

export class GherkinSupportException extends Error {
}

const supportStore = support.createSupportStore();

const validateSupportResult = (res) => {
    if (isLeft(res)) {
        const message = fromLeft({})(res);
        throw new GherkinSupportException(message);
    }
};


(function setupGlobal(ctx) {
    const gherkinSupport = {
        executeGiven: (label, stepData) => validateSupportResult(support.executeGiven(supportStore)(label)(stepData)()),
        executeWhen: (label, stepData) => validateSupportResult(support.executeWhen(supportStore)(label)(stepData)()),
        executeThen: (label, stepData) => validateSupportResult(support.executeThen(supportStore)(label)(stepData)()),
        executeAnd: (label, stepData) => validateSupportResult(support.executeAnd(supportStore)(label)(stepData)()),
        executeBut: (label, stepData) => validateSupportResult(support.executeBut(supportStore)(label)(stepData)()),
        excuteHookBeforeFeature: (tags) => support.excuteHookBeforeFeature(supportStore)(tags)(),
        excuteHookAfterFeature: (tags) => support.excuteHookAfterFeature(supportStore)(tags)(),
        excuteHookBeforeExample: (tags) => support.excuteHookBeforeExample(supportStore)(tags)(),
        excuteHookAfterExample: (tags) => support.excuteHookAfterExample(supportStore)(tags)(),
        excuteHookBeforeStep: (tags) => support.excuteHookBeforeStep(supportStore)(tags)(),
        excuteHookAfterStep: (tags) => support.excuteHookAfterStep(supportStore)(tags)()
    };

    ctx.gherkinSupport = gherkinSupport;
}(global || window));



const executeUserStepFn = (fn) => (matches) => (stepData) => () => fn.apply(null, [...matches, stepData]);

const regexOrExpressionToRegisterable = (regexOrExpression) => {
    if(typeof regexOrExpression === 'string') {
        return new RegisterStepTestExpression(regexOrExpression);
    } else {
        return new RegisterStepTestRegex(regexOrExpression);
    }
}

export function Given(regexOrExpression, fn) {
    validateSupportResult(support.givenStep(supportStore)(regexOrExpressionToRegisterable(regexOrExpression))(executeUserStepFn(fn))());
}

export function When(regexOrExpression, fn) {
    validateSupportResult(support.whenStep(supportStore)(regexOrExpressionToRegisterable(regexOrExpression))(executeUserStepFn(fn))());
}

export function Then(regexOrExpression, fn) {
    validateSupportResult(support.thenStep(supportStore)(regexOrExpressionToRegisterable(regexOrExpression))(executeUserStepFn(fn))());
}

export function And(regexOrExpression, fn) {
    validateSupportResult(support.andStep(supportStore)(regexOrExpressionToRegisterable(regexOrExpression))(executeUserStepFn(fn))());
}

export function But(regexOrExpression, fn) {
    validateSupportResult(support.butStep(supportStore)(regexOrExpressionToRegisterable(regexOrExpression))(executeUserStepFn(fn))());
}

const executeUserHookFn = (fn) => (tags) => () => fn(tags);

export function beforeFeature(fn) {
    validateSupportResult(support.registerBeforeFeatureHook(supportStore)(Nothing.value)(executeUserHookFn(fn))());
}

export function afterFeature(fn) {
    validateSupportResult(support.registerAfterFeatureHook(supportStore)(Nothing.value)(executeUserHookFn(fn))());
}

export function beforeScenario(fn) {
    validateSupportResult(support.registerBeforeExampleHook(supportStore)(Nothing.value)(executeUserHookFn(fn))());
}

export function afterScenario(fn) {
    validateSupportResult(support.registerAfterExampleHook(supportStore)(Nothing.value)(executeUserHookFn(fn))());
}

export function beforeStep(fn) {
    validateSupportResult(support.registerBeforeStepHook(supportStore)(Nothing.value)(executeUserHookFn(fn))());
}

export function afterStep(fn) {
    validateSupportResult(support.registerAfterStepHook(supportStore)(Nothing.value)(executeUserHookFn(fn))());
}

export function beforeTaggedFeature(tagExp, fn) {
    validateSupportResult(support.registerBeforeFeatureHook(supportStore)(new Just(tagExp))(executeUserHookFn(fn))());
}

export function afterTaggedFeature(tagExp, fn) {
    validateSupportResult(support.registerAfterFeatureHook(supportStore)(new Just(tagExp))(executeUserHookFn(fn))());
}

export function beforeTaggedScenario(tagExp, fn) {
    validateSupportResult(support.registerBeforeExampleHook(supportStore)(new Just(tagExp))(executeUserHookFn(fn))());
}

export function afterTaggedScenario(tagExp, fn) {
    validateSupportResult(support.registerAfterExampleHook(supportStore)(new Just(tagExp))(executeUserHookFn(fn))());
}

export function beforeTaggedStep(tagExp, fn) {
    validateSupportResult(support.registerBeforeStepHook(supportStore)(new Just(tagExp))(executeUserHookFn(fn))());
}

export function afterTaggedStep(tagExp, fn) {
    validateSupportResult(support.registerAfterStepHook(supportStore)(new Just(tagExp))(executeUserHookFn(fn))());
}

export function defineCucumberExpressionToken(tokenName, tokenRegex, transformFn) {
    support.defineCucumberExpressionToken(supportStore)(tokenName)(tokenRegex)(transformFn)();
}
