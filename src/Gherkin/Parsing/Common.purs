module Gherkin.Parsing.Common where


import Prelude

import Control.Alt ((<|>))
import Data.Foldable (foldMap)
import Data.String as JStr
import Data.String.CodeUnits as Str
import Text.Parsing.StringParser (Parser)
import Text.Parsing.StringParser as PR
import Text.Parsing.StringParser.Combinators ((<?>))
import Text.Parsing.StringParser.Combinators as PC
import Text.Parsing.StringParser.String as P


newlineP :: Parser Unit
newlineP = void $ P.regex "\\r?\\n" <?> "Expected newline"

eolP :: Parser Unit
eolP = P.eof <|> newlineP

indentP :: Parser Int
indentP = JStr.length <$> P.regex "[ \t]*"

expectIndentP :: Int -> Parser Unit
expectIndentP len = do
  nowLen <- indentP
  if nowLen < len 
    then PR.fail $ "Expected indent of " <> show len <> " but got " <> show nowLen
    else pure unit

stringTillEolP :: Parser String
stringTillEolP = stringTillP eolP

stringTillNewlineP :: Parser String
stringTillNewlineP = stringTillP newlineP

stringTillP :: ∀ a. Parser a -> Parser String
stringTillP end = foldMap Str.singleton <$> PC.manyTill P.anyChar end

data Escaped = Escaped Char | Raw Char

unescape :: Escaped -> Char
unescape (Escaped a) = a
unescape (Raw a) = a

escapedP :: Parser Char -> Parser Escaped
escapedP s = Escaped <$> (P.char '\\' *> s) <|> Raw <$> s

onlyEscapedP :: Parser Char -> Parser Char
onlyEscapedP s = PR.try $ escapedP s >>= case _ of
    Escaped c -> pure c
    Raw c -> PR.fail $ "Expected '" <> show c <> "' to be escaped" 
