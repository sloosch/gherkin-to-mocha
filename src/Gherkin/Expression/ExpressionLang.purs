module Gherkin.Expression.ExpressionLang (langP) where

import Prelude

import Control.Monad.State (StateT, evalStateT, lift)
import Control.Monad.State as State
import Data.Foldable (intercalate)
import Data.List (List, (:))
import Data.List as List
import Data.Maybe (Maybe(..))
import Text.Parsing.StringParser (Parser)
import Text.Parsing.StringParser as PR
import Text.Parsing.StringParser.Combinators ((<?>))
import Text.Parsing.StringParser.Combinators as PC
import Text.Parsing.StringParser.String as P


data Operator a = BinaryOp (a -> a -> a) | UnaryOp (a -> a) | OpenBracket | CloseBracket
instance showOperators :: Show (Operator a) where
    show (BinaryOp _) = "BinaryOp"
    show (UnaryOp _) = "UnaryOp"
    show (OpenBracket) = "OpenBracket"
    show (CloseBracket) = "CloseBracket"

data Lang a = Lit a | Op (Operator a) 


type State a = {output:: List a, operators :: List (Operator a)}

-- shunting-yard where each operator has the same precedence
langP :: ∀ a. List (Parser a) -> List (Parser (a -> a)) -> List (Parser (a -> a -> a)) -> Parser a
langP lits unaryOps binaryOps = flip evalStateT {output: List.Nil, operators: List.Nil} loop
    where
    loop :: StateT (State a) Parser a
    loop = do
        parseNextToken
        eof <- lift $ PC.optionMaybe $ PR.try $ PC.lookAhead $ P.skipSpaces *> P.eof
        case eof of
            Nothing -> loop
            Just _ -> do
                let popTillEmpty continue
                        | continue = popAndApplyOperator >>= popTillEmpty
                        | otherwise = pure unit
                popTillEmpty true
                {output, operators} <- State.get
                if List.length operators /= 0
                    then lift $ PR.fail $ "Syntax error: Dangling operator: " <> (intercalate ", " $ show <$> operators)
                    else case List.uncons output of
                        Just {head, tail: List.Nil} -> pure head
                        Nothing -> lift $ PR.fail $ "Nothing to parse"
                        _ -> lift $ PR.fail $ "Syntax error: Dangling literal"

    parseNextToken :: StateT (State a) Parser Unit
    parseNextToken = do
        lang <- lift $ P.skipSpaces *> PC.choice [
            Lit <$> PR.try (PC.choice lits),
            (Op <<< UnaryOp) <$> PR.try (PC.choice unaryOps),
            (Op <<< BinaryOp) <$> PR.try (PC.choice binaryOps),
            (Op OpenBracket) <$ PR.try (P.char '('),
            (Op CloseBracket) <$ PR.try (P.char ')')
        ] <?> "Unexpected Token"

        let pushOp op = State.modify_ \s -> s{operators = op : s.operators}
        case lang of
            Lit exp -> State.modify_ \s -> s{output = exp : s.output}
            Op op -> case op of
                UnaryOp _ -> pushOp op
                BinaryOp _ -> popAndApplyOperator *> pushOp op
                OpenBracket -> pushOp op
                CloseBracket ->
                    let popTillOpenBracket = do
                            _ <- popAndApplyOperator
                            State.gets (List.uncons <<< _.operators) >>= case _ of
                                Just {head: OpenBracket, tail} -> do
                                    State.modify_ _{operators = tail}
                                    pure true
                                Nothing -> pure false
                                _ -> popTillOpenBracket
                    in do
                        found <- popTillOpenBracket
                        if not found
                            then lift $ PR.fail "Missing open bracket"
                            else pure unit

    popAndApplyOperator :: StateT (State a) Parser Boolean
    popAndApplyOperator = do
        opers <- State.gets _.operators
        case List.uncons opers of
            Just {head: (BinaryOp g), tail: restOpers} -> do
                out <- State.gets _.output
                let vals = do
                        {head: rhs, tail} <- List.uncons out
                        {head: lhs, tail: rest} <- List.uncons tail
                        pure {lhs, rhs, rest}
                case vals of
                    Nothing -> lift $ PR.fail "Could not apply a binary op: missing arguments"
                    Just {lhs, rhs, rest} -> do
                        let ap = g lhs rhs
                        State.put {output:  ap : rest, operators: restOpers}
                pure true
            Just {head: (UnaryOp g), tail: restOpers} -> do
                out <- State.gets _.output
                case List.uncons out of
                    Nothing -> lift $ PR.fail "Could not apply an unary op: missing argument"
                    Just {head,tail} -> do
                        let ap = g head
                        State.put {output: ap : tail, operators: restOpers}
                pure true
            _ -> pure false
