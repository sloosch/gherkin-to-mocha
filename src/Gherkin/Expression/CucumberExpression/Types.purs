module Gherkin.Expression.CucumberExpression.Types where

import Prelude

import Foreign (Foreign)
import Text.Parsing.StringParser (Parser)
import Unsafe.Coerce (unsafeCoerce)


type ExpressionString = String

foreign import data Any :: Type

type TokenName = String
type TokenRegex = String
type TokenTransformFn = String -> Any

newtype CukeToken = CukeToken {
    name :: TokenName,
    regex :: TokenRegex,
    transform :: TokenTransformFn
}

instance showCukeToken :: Show CukeToken where
    show (CukeToken token) = "CukeToken " <> show token.name <> " " <> show token.regex

toAny :: ∀ a. a -> Any
toAny = unsafeCoerce

anyToForeign :: Any -> Foreign
anyToForeign = unsafeCoerce


data CukeExpression = CukeExpression String (Parser (Array Any))
instance showCukeExpression :: Show CukeExpression where
    show (CukeExpression str _) = "CukeExpression " <> show str
