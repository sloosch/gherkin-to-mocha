module Gherkin.Expression.CucumberExpression (
    compile, 
    match
) where

import Prelude

import Control.Alt ((<|>))
import Control.Monad.Rec.Class (Step(..), tailRecM)
import Data.Array (foldMap, intercalate)
import Data.Array as Arr
import Data.Bifunctor (bimap, lmap)
import Data.Either (Either)
import Data.Foldable (class Foldable, foldM)
import Data.List (List(..), (:))
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String.CodeUnits as Str
import Gherkin.Expression.CucumberExpression.Types (Any, CukeExpression(..), CukeToken(..), ExpressionString)
import Gherkin.Parsing.Common (onlyEscapedP, stringTillP)
import Text.Parsing.StringParser (Parser)
import Text.Parsing.StringParser as PR
import Text.Parsing.StringParser.Combinators ((<?>))
import Text.Parsing.StringParser.Combinators as PC
import Text.Parsing.StringParser.String as P

data Lang = 
      Match String
    | MatchCukeToken CukeToken
    | OptionalText String
    | AlternativeText (List String)

instance showLang :: Show Lang where
    show (Match s) = "Match " <> show s
    show (MatchCukeToken t) = "MatchCukeToken " <> show t
    show (OptionalText t) = "OptionalText " <> show t
    show (AlternativeText t) = "AlternativeText " <> show t


cukeExpP :: ∀ f. Foldable f => Functor f => f CukeToken -> Parser CukeToken
cukeExpP expr = do
    PC.between 
        (P.char '{') (P.char '}' <?> errMsg) 
        ((PC.choice $ expr <#> \q@(CukeToken e) -> (P.string e.name) $> q) <?> errMsg)
    
    where
    errMsg = "Could not find any matching expression token. Valid tokens are: " <> (intercalate ", " $ expr <#> \(CukeToken {name}) -> name)

optionalTextP :: Parser String
optionalTextP =  P.char '(' *> stringTillP (P.char ')')

alternativeTextP :: Parser (List String)
alternativeTextP = PR.try do
    prev <- whiteSpaceFreeTextP <* P.char '/'
    next <- whiteSpaceFreeTextP
    tail <- PC.optionMaybe $ PR.try $ P.char '/' *> alternativeTextP

    pure $ prev : next : fromMaybe Nil tail 

    where
    whiteSpaceFreeTextP :: Parser String
    whiteSpaceFreeTextP = map (foldMap Str.singleton) $ PC.many1 (P.noneOf [' ', '\t', '\r', '\n', '/'])

cukeP :: ∀ f.Foldable f => Functor f => f CukeToken -> Parser (Array Lang)
cukeP expr = flip tailRecM [] \a -> do
    PC.choice [
        Done a <$ P.eof,
        (Loop <<< Arr.snoc a <<< MatchCukeToken) <$> cukeExpP expr,
        (Loop <<< Arr.snoc a <<< OptionalText) <$> optionalTextP,
        (Loop <<< Arr.snoc a <<< AlternativeText) <$> alternativeTextP,
        (Loop <<< Arr.snoc a <<< Match) <$> stringTillNoneEscapedP ['{', '(']
    ]
    where
    stringTillNoneEscapedP :: ∀ g. Foldable g => g Char -> Parser String
    stringTillNoneEscapedP esc = map (foldMap Str.singleton) $ PC.many do
        altText <- PC.optionMaybe $ PC.lookAhead $ alternativeTextP
        case altText of
            Just _ -> PR.fail "found alternative text"
            Nothing -> onlyEscapedP (P.oneOf esc) <|> P.noneOf esc
    

compile :: ∀ f.Foldable f => Functor f => f CukeToken -> ExpressionString -> Either String CukeExpression
compile expr str = 
    bimap 
        (append ("Could not compile cucumber expression '" <> str <> "':") <<< show) 
        (CukeExpression str)
        $ PR.runParser (foldM langToParser [] <$> cukeP expr) str
    where
    langToParser a e = case e of
        Match s -> P.string s $> a
        MatchCukeToken (CukeToken t) -> Arr.snoc a <$> (t.transform <$> P.regex t.regex)
        OptionalText text -> (PC.optional $ P.string text) $> a
        AlternativeText text -> (PC.choice $ P.string <$> text) $> a


match :: CukeExpression -> String -> Either String (Array Any)
match (CukeExpression str expr) s = lmap (append ("Could not match expression '" <> str <> "':") <<< show) $ PR.runParser expr s
