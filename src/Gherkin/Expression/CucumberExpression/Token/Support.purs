module Gherkin.Expression.CucumberExpression.Token.Support where

import Prelude

import Data.Array as Arr
import Effect (Effect)
import Effect.Ref (Ref)
import Effect.Ref as Ref
import Gherkin.Expression.CucumberExpression.Token as Token
import Gherkin.Expression.CucumberExpression.Types (CukeToken(..), TokenName, TokenRegex, TokenTransformFn)

newtype TokenStore = TokenStore (Ref (Array CukeToken))

createStore :: Effect (TokenStore)
createStore = TokenStore <$> Ref.new []

defineToken :: TokenStore -> TokenName -> TokenRegex -> TokenTransformFn -> Effect Unit
defineToken (TokenStore store) name regex transform =
    let token = CukeToken {name, regex, transform} in
    Ref.modify_ (Arr.cons token) store

allTokens :: TokenStore -> Effect (Array CukeToken)
allTokens (TokenStore store) = do
    definedTokens <- Ref.read store
    pure $ Token.default <> definedTokens
