module Gherkin.Expression.CucumberExpression.Token where

import Prelude

import Data.Int as Int
import Data.Maybe (fromJust)
import Data.Number as Number
import Data.String.CodeUnits as Str
import Gherkin.Expression.CucumberExpression.Types (CukeToken(..), toAny)
import Partial.Unsafe (unsafePartialBecause)


word :: CukeToken
word = CukeToken {
    name: "word",
    regex: "\\w+",
    transform: toAny
}

string :: CukeToken
string = CukeToken {
    name: "string",
    regex: "['\"](.+?)['\"]",
    transform: toAny <<< Str.drop 1 <<< Str.dropRight 1
}

int :: CukeToken
int = CukeToken {
    name: "int",
    regex: "\\d+",
    transform: unsafePartialBecause "It should be a valid int" $ toAny <<< fromJust <<< Int.fromString
}

float :: CukeToken
float = CukeToken {
    name: "float",
    regex: "\\d+?\\.(\\d+)?",
    transform: unsafePartialBecause "It should be a valid number" $ toAny <<< fromJust <<< Number.fromString
}

default :: Array CukeToken
default = [word, string, int, float]
