module Gherkin.Expression.TagExpression (compile, eval, Expression, ExpressionString) where

import Prelude

import Data.Bifunctor (lmap)
import Data.Either (Either)
import Data.Foldable (class Foldable, elem)
import Data.List as List
import Gherkin (Tag, tagP)
import Gherkin.Expression.ExpressionLang (langP)
import Text.Parsing.StringParser (Parser, runParser)
import Text.Parsing.StringParser as PR
import Text.Parsing.StringParser.String as P

type ExpressionString = String

data Expression =
      Contains Tag
    | And Expression Expression
    | Or Expression Expression
    | Not Expression

andP :: Parser (Expression -> Expression -> Expression)
andP =  PR.try $ And <$ P.string "and"

orP :: Parser (Expression -> Expression -> Expression)
orP = PR.try $ Or <$ P.string "or"

notP :: Parser (Expression -> Expression)
notP = PR.try $ Not <$ P.string "not"

containsTagP :: Parser Expression
containsTagP = PR.try $ Contains <$> tagP

expressionP :: Parser Expression
expressionP = langP (List.singleton containsTagP) (List.singleton notP) (List.fromFoldable [andP, orP])

compile :: ExpressionString -> Either String Expression
compile str = lmap show $ runParser expressionP str

eval :: ∀ f. Foldable f => Expression -> f Tag -> Boolean
eval exp tags = go exp
    where
    go :: Expression -> Boolean
    go (Contains t) = elem t tags
    go (And a b) = (go a) && (go b)
    go (Or a b) = (go a) || (go b)
    go (Not a) = not $ go a
