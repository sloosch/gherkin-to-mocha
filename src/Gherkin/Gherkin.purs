module Gherkin (
    parse,
    Feature(..),
    Background(..),
    Tag(..),
    Example(..),
    DataTable(..),
    DocString(..),
    Step(..),
    StepType(..),
    StepData(..),
    tagP
) where

import Prelude

import Control.Alt ((<|>))
import Data.Array (intercalate)
import Data.Either (Either, fromRight)
import Data.List (List)
import Data.List as List
import Data.List.NonEmpty as NEL
import Data.List.Types (NonEmptyList)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Newtype (class Newtype)
import Data.Semigroup.Foldable (intercalateMap)
import Data.String as JStr
import Data.String.Regex (Regex)
import Data.String.Regex as Regex
import Data.String.Regex.Flags as RegexFlags
import Gherkin.Parsing.Common (eolP, expectIndentP, indentP, newlineP, stringTillEolP, stringTillNewlineP, stringTillP)
import Partial.Unsafe (unsafePartialBecause)
import Text.Parsing.StringParser (ParseError, Parser, runParser)
import Text.Parsing.StringParser as PR
import Text.Parsing.StringParser.Combinators as PC
import Text.Parsing.StringParser.String as P

newtype Tag = Tag String
derive instance newTypeTag :: Newtype Tag _
derive newtype instance eqTag :: Eq Tag
instance showTag :: Show Tag where
  show (Tag str) = "Tag " <> str

newtype Feature = Feature {
  label :: Maybe String,
  background :: Maybe Background,
  examples :: List Example,
  tags :: List Tag,
  description :: Maybe String
}
derive instance newtypeFeature :: Newtype Feature _
derive instance eqFeature :: Eq Feature
instance showFeature :: Show Feature where
  show (Feature {label,background, examples, tags, description}) =
    "Feature " <> show label <> " " <> show background <> " " <> show examples <> " " <> show tags <> " " <> show description

newtype Example = Example {
  label :: Maybe String,
  steps :: List Step,
  tags :: List Tag,
  table :: Maybe DataTable,
  description :: Maybe String
}
derive instance newtypeExample :: Newtype Example _
derive instance eqExample :: Eq Example
instance showExample :: Show Example where
  show (Example {label, steps, tags, table, description}) = 
    "Example " <> show label <> " " <> show steps <> " " <> show tags <> " " <> show table <> " "  <>show description
  
newtype Background = Background {
  label :: Maybe String,
  steps :: List Step,
  description :: Maybe String
} 
derive instance newtypeBackground :: Newtype Background _
derive instance eqBackground :: Eq Background
instance showBackground :: Show Background where
  show (Background {label, steps, description}) =
    "Background " <> show label <> " " <> show steps  <> " " <> show description

newtype DocString = DocString String
derive instance newtypeDocString :: Newtype DocString _
derive newtype instance eqDocString :: Eq DocString
instance showDocString :: Show DocString where
  show (DocString str) = "DocString " <> str 

data DataTable = DataTable (NonEmptyList String) (NonEmptyList (NonEmptyList String))
derive instance eqDataTable :: Eq DataTable
instance showDataTable :: Show DataTable where
  show (DataTable headers rows) = "DataTable " <> (intercalate "|" headers) <> "\n" <> (intercalateMap "\n" (intercalate "|") rows)

data StepType = Given | When | Then | And | But
derive instance eqStepType :: Eq StepType
instance showStepType :: Show StepType where
  show Given = "Given"
  show When = "When"
  show Then = "Then"
  show And = "And"
  show But = "But"

data StepData = NoStepData | StepWithDocString DocString | StepWithDataTable DataTable
derive instance eqStepData :: Eq StepData
instance showStepData :: Show StepData where
  show NoStepData = "NoStepData"
  show (StepWithDocString str) = "StepWithDocString " <> show str
  show (StepWithDataTable table) = "StepWithDataTable " <> show table
newtype Step = Step {
  type :: StepType,
  label :: String,
  data :: StepData
}
derive instance newtypeStep :: Newtype Step _
derive instance eqStep :: Eq Step
instance showStep :: Show Step where
  show (Step step) = "Step {type: " <> show step.type <> ", label: " <> step.label <> ", data: " <> show step.data <> "}"

tagP :: Parser Tag
tagP = Tag <$> (P.char '@' *> stringTillP (void sep <|> eolP))
  where
  sep = PR.try $ PC.choice [P.char ' ', PC.lookAhead $ P.char ')']

descriptionP :: Parser (Maybe String)
descriptionP = do
  let collectTillStop = do
        s <- PC.optionMaybe stopP
        case s of
          Nothing -> do
            head <- stringTillEolP
            end <- collectTillStop
            pure $ List.Cons head end
          Just _ -> pure $ List.Nil
  
  desc <- collectTillStop
  pure do
    lines <- NEL.fromFoldable desc
    let firstLine = NEL.head lines
    let firstIndent = indentLen firstLine
    pure $ 
      intercalateMap "\n" 
      (\line -> let i = min (indentLen line) firstIndent in JStr.drop i line)
      lines
  where
  stopP = PC.lookAhead $ PR.try $ 
    P.skipSpaces *> (
        void backgroundKeywordP <|> void exampleKeywordsP <|> void tagP <|> void stepKeywordsP <|> P.eof
      )
  indentLen  = let space = JStr.codePointFromChar ' ' in JStr.countPrefix (_ == space)

featureP :: Parser Feature
featureP = do
    P.skipSpaces
    tags <- PC.many tagP
    P.skipSpaces
    _ <- P.string "Feature:"
    label <- PC.optionMaybe (JStr.trim <$> stringTillEolP)
    description <- descriptionP
    P.skipSpaces
    background <- PC.optionMaybe backgroundP
    P.skipSpaces
    examples <- PC.many (exampleP <* P.skipSpaces)
    P.skipSpaces
    P.eof

    pure $ Feature {label, background, examples, tags, description: description}

backgroundKeywordP :: Parser String
backgroundKeywordP = P.string "Background:"

backgroundP :: Parser Background
backgroundP = do
    _ <- PR.try backgroundKeywordP
    label <- PC.optionMaybe (JStr.trim <$> stringTillEolP)
    description <- descriptionP
    P.skipSpaces
    steps <- stepsP

    pure $ Background {label, steps, description}

examplesTableP :: Parser DataTable
examplesTableP = PR.try $ do
  tags <- PC.many tagP
  P.skipSpaces
  _ <- P.string "Examples:"
  P.skipSpaces
  dataTableP

exampleKeywordsP :: Parser String
exampleKeywordsP = PC.choice $ P.string <$> ["Scenario:", "Scenario Outline:"]

exampleP :: Parser Example
exampleP = do
    tags <- PC.many tagP
    P.skipSpaces
    _ <- exampleKeywordsP
    label <- PC.optionMaybe (JStr.trim <$> stringTillEolP)
    description <- descriptionP
    steps <- stepsP
    P.skipSpaces
    table <- PC.optionMaybe examplesTableP

    pure $ Example {label, steps, tags, table, description: description}

docStringP :: Parser DocString
docStringP =  do
  let start = PR.try do
        indent <- indentP
        quotes <- PC.optionMaybe docStringQuotesP
        newlineP
        case quotes of
          Just _ -> pure indent
          Nothing -> start
  indent <- start
  lines <- PC.manyTill (expectIndentP indent *> stringTillNewlineP) (PR.try $ expectIndentP indent *> docStringQuotesP)

  pure $ DocString $ intercalate "\n" lines
  where
  docStringQuotesP :: Parser Unit
  docStringQuotesP = void $ P.string "\"\"\""

dataTableP :: Parser DataTable
dataTableP = do
  headerRow <- PR.try (P.skipSpaces *> dataTableRowP)
  rows <- PC.many1 (PR.try dataTableRowP)
  let headerLen = NEL.length headerRow
  let diffs = NEL.elemIndex false $ (eq headerLen <<< NEL.length) <$> rows
  case diffs of
    Just idx -> PR.fail $ "Datatable at row " <> show idx <> " does not have the same column count as the header"
    Nothing -> pure $ DataTable headerRow rows
  where
  dataTableRowP :: Parser (NonEmptyList String)
  dataTableRowP =  do
    _ <- indentP
    _ <- P.char '|'
    cells <- PC.many1Till (stringTillP $ P.char '|' <* indentP) eolP

    pure $ JStr.trim <$> cells


stepKeywordsP :: Parser String
stepKeywordsP = PC.choice $ P.string <$> ["Given ", "When ", "Then ", "And ", "But "]

stepsP :: Parser (List Step)
stepsP = PC.many (PC.choice [givenP, whenP, thenP, andP, butP])
  where

  stepP :: StepType -> String -> Parser Step
  stepP ty stepLabel = do
    void $ PR.try $ P.skipSpaces *> (P.string $ stepLabel <> " ")
    def <- stringTillEolP
    dat <- PC.optionMaybe (StepWithDocString <$> docStringP <|> StepWithDataTable <$> dataTableP)
    let stepData = fromMaybe NoStepData dat

    pure $ Step {type: ty, data: stepData, label: def}

  givenP :: Parser Step
  givenP = stepP Given "Given"

  whenP :: Parser Step
  whenP = stepP When "When"

  thenP :: Parser Step
  thenP = stepP Then "Then"

  andP :: Parser Step
  andP = stepP And "And"

  butP :: Parser Step
  butP = stepP But "But"


parse :: String -> Either ParseError Feature
parse = runParser featureP <<< removeComments
  where
  removeComments :: String -> String
  removeComments = Regex.replace commentRegex ""

  commentRegex :: Regex
  commentRegex = unsafePartialBecause "it is a static regex" $ fromRight $ Regex.regex "#.*" RegexFlags.global
