module Gherkin.Transform.Support (transformWithForeignConfig) where

import Prelude

import Data.Bifunctor (lmap)
import Data.Either (Either)
import Foreign (Foreign)
import Gherkin as Gherkin
import Gherkin.Transform.Configuration as TranformConfiguration
import Gherkin.Transform.Types (class TransformTo, transformGherkin)

transformWithForeignConfig :: ∀ a. TransformTo a => Foreign -> String -> Either String a
transformWithForeignConfig fconfig featureStr = do
    config <- TranformConfiguration.fromForeign fconfig
    feature <- lmap show $ Gherkin.parse featureStr

    pure $ transformGherkin config feature
