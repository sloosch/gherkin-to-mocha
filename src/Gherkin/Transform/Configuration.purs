module Gherkin.Transform.Configuration where

import Prelude
import Control.Monad.Except (except, runExcept)
import Data.Bifunctor (lmap)
import Data.Either (Either)
import Data.Maybe (Maybe(..))
import Data.Newtype (unwrap)
import Data.Semigroup.Foldable (intercalateMap)
import Foreign (Foreign, ForeignError(..))
import Foreign as Foreign
import Foreign.Index as ForeignI
import Gherkin.Expression.TagExpression as TX
import Gherkin.Transform.Types (TransformConfiguration(..))

defaultConfig :: TransformConfiguration
defaultConfig = TransformConfiguration {
    pathToStepDefintions:  "../support/step_definitions",
    skipTagExpression: Nothing,
    onlyTagExpression: Nothing
}

fromForeign :: Foreign -> Either String TransformConfiguration
fromForeign fconfig
     | Foreign.isNull fconfig || Foreign.isUndefined fconfig = pure defaultConfig
     | otherwise = 
        lmap (append "configuration error: " <<< intercalateMap ", " show) $ runExcept do
            let compatCompile = except <<< lmap (pure <<< ForeignError <<< append "tag expression: ") <<< TX.compile
            pathToStepDefintions <- if ForeignI.hasOwnProperty "pathToStepDefintions" fconfig
                then ForeignI.readProp "pathToStepDefintions" fconfig >>= Foreign.readString
                else pure $ _.pathToStepDefintions $ unwrap defaultConfig
            skipTagExpression <- if ForeignI.hasOwnProperty "skipTagExpression" fconfig
                then Just <$> (ForeignI.readProp "skipTagExpression" fconfig >>= Foreign.readString >>= compatCompile)
                else pure $ _.skipTagExpression $ unwrap defaultConfig
            onlyTagExpression <- if ForeignI.hasOwnProperty "onlyTagExpression" fconfig
                then Just <$> (ForeignI.readProp "onlyTagExpression" fconfig >>= Foreign.readString >>= compatCompile)
                else pure $ _.onlyTagExpression $ unwrap defaultConfig
            
            pure $ TransformConfiguration {pathToStepDefintions, skipTagExpression, onlyTagExpression}
