module Gherkin.Transform.AST where

import Gherkin (Feature)
import Gherkin.Transform.Types (class TransformTo, TransformConfiguration)

newtype AST = AST {
    configuration :: TransformConfiguration,
    feature :: Feature
}

instance transformToAst :: TransformTo AST where
    transformGherkin configuration feature = AST {configuration, feature}
