module Gherkin.Transform.Mocha (Mocha) where

import Prelude

import Control.Monad.Reader (class MonadAsk, asks, runReaderT)
import Control.Monad.Writer (class MonadTell, execWriter, tell)
import Data.Array as Arr
import Data.Foldable (class Foldable, for_, intercalate)
import Data.FoldableWithIndex (foldrWithIndex)
import Data.List (List)
import Data.List as List
import Data.List.NonEmpty as NEL
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Newtype (unwrap)
import Data.Semigroup.Foldable (intercalateMap)
import Data.String as Str
import Gherkin (Background(..), DataTable(..), DocString(..), Example(..), Feature(..), Step(..), StepData(..), StepType(..), Tag)
import Gherkin.Expression.TagExpression as TX
import Gherkin.Transform.Types (class TransformTo, TransformConfiguration)

type JS = String

newtype Mocha = Mocha JS

instance transformToMocha :: TransformTo Mocha where
    transformGherkin = transform

foreign import stringifyString :: String -> String

data DescribeType = Describe | DescribeSkip | DescribeOnly

describeTypeFromTags :: ∀ m. MonadAsk TransformConfiguration m => List Tag -> m DescribeType
describeTypeFromTags tags = do
    config <- asks unwrap
    pure case config.skipTagExpression of
        Just expr | TX.eval expr tags -> DescribeSkip
        _ -> case config.onlyTagExpression of
            Just expr | TX.eval expr tags -> DescribeOnly
            _ -> Describe
            
describeTypeToJs :: DescribeType -> JS
describeTypeToJs (Describe) = "describe"
describeTypeToJs (DescribeSkip) = "describe.skip"
describeTypeToJs (DescribeOnly) = "describe.only"

toJsArray :: ∀ f. Foldable f => Functor f => f String -> JS
toJsArray cells = "[" <> (intercalate ", " $ stringifyString <$> cells) <> "]"

stepDataToJs :: StepData -> JS
stepDataToJs NoStepData = "null"
stepDataToJs (StepWithDocString (DocString str)) = stringifyString str
stepDataToJs (StepWithDataTable (DataTable headers rows)) = 
    intercalate "\n" [
        "{",
        "headers: " <> (toJsArray headers) <> ",",
        "rows: [" <> (intercalateMap ",\n" toJsArray rows) <> "]",
        "}"
    ]


describe :: ∀ m. MonadTell (Array JS) m => DescribeType -> String -> m Unit -> m Unit
describe describeType label m =
    tell [describeTypeToJs describeType <> "(" <> stringifyString label <> ", () => {"] *> m *> tell ["});"] 

import' :: ∀ m. MonadTell (Array JS) m => String -> m Unit 
import' i = tell ["import " <> stringifyString i <> ";"]

before :: ∀ m. MonadTell (Array JS) m => m Unit -> m Unit
before m = tell ["before(() => {"] *> m *> tell ["});"]

after :: ∀ m. MonadTell (Array JS) m => m Unit -> m Unit
after m = tell ["after(() => {"] *> m *> tell ["});"]

beforeEach :: ∀ m. MonadTell (Array JS) m => m Unit -> m Unit
beforeEach m = tell ["beforeEach(() => {"] *> m *> tell ["});"]

afterEach :: ∀ m. MonadTell (Array JS) m => m Unit -> m Unit
afterEach m = tell ["afterEach(() => {"] *> m *> tell ["});"]

transform :: TransformConfiguration -> Feature -> Mocha
transform config (Feature feature) = Mocha $ intercalate "\n" $ execWriter $ flip runReaderT config do
    pathToStepDefintions <- asks $ _.pathToStepDefintions <<< unwrap

    import' pathToStepDefintions

    describeType <- describeTypeFromTags feature.tags
    let label = fromMaybe "Feature:" feature.label

    describe describeType label do
        let tagsAsJsArray = toJsArray $ unwrap <$> feature.tags
        before $ tell ["gherkinSupport.excuteHookBeforeFeature(" <> tagsAsJsArray <> ");"]
        after $ tell ["gherkinSupport.excuteHookAfterFeature(" <> tagsAsJsArray <> ");"]

        for_ feature.examples $ forScenarioData feature.tags $ scenarioCode feature.background
    
scenarioCode :: ∀ m. MonadTell (Array JS) m => Maybe Background -> String -> DescribeType -> List Step -> List Tag -> m Unit
scenarioCode background exLabel describeType steps tags = do
    describe describeType exLabel do
        let tagsAsJsArray = toJsArray $ unwrap <$> tags
        before $ tell ["gherkinSupport.excuteHookBeforeExample(" <> tagsAsJsArray <> ");"]
        after $ tell ["gherkinSupport.excuteHookAfterExample(" <> tagsAsJsArray <> ");"]
        beforeEach $ tell ["gherkinSupport.excuteHookBeforeStep(" <> tagsAsJsArray <> ");"] 
        afterEach $ tell ["gherkinSupport.excuteHookAfterStep(" <> tagsAsJsArray <> ");"] 
        case background of
            Just (Background {steps: backgroundSteps}) -> before $ for_ backgroundSteps stepSupportCode
            Nothing -> pure unit
        for_ steps itStep

stepSupportCode :: ∀ m. MonadTell (Array JS) m => Step -> m Unit
stepSupportCode (Step step) =
    let label = stringifyString step.label 
        stepDataJs = stepDataToJs step.data
    in do
    case step.type of
        Given -> tell ["gherkinSupport.executeGiven(" <> label <> ", " <> stepDataJs <> ");"]
        When -> tell ["gherkinSupport.executeWhen(" <> label <> ", " <> stepDataJs <> ");"]
        Then -> tell ["gherkinSupport.executeThen(" <> label <> ", " <> stepDataJs <> ");"]
        And -> tell ["gherkinSupport.executeAnd(" <> label <> ", " <> stepDataJs <> ");"]
        But -> tell ["gherkinSupport.executeBut(" <> label <> ", " <> stepDataJs <> ");"]

itStep :: ∀ m. MonadTell (Array JS) m => Step -> m Unit
itStep s@(Step step) = do
    let label = stringifyString $ (show step.type) <> " " <> step.label
    tell ["it(" <> label <> ", () => {"]
    stepSupportCode s
    tell ["});"] 

forScenarioData :: ∀ m. MonadAsk TransformConfiguration m => List Tag -> (String -> DescribeType -> List Step -> List Tag -> m Unit) -> Example -> m Unit
forScenarioData featTags f (Example example) =
    let uniqTags = List.nub (featTags <> example.tags)

    in do
    describeType <- describeTypeFromTags example.tags
    case example.table of
        Nothing -> f exLabel describeType example.steps uniqTags
        Just (DataTable headers rows) -> do 
            let headerArr = Arr.fromFoldable headers
            for_ rows \row -> do
                let replaceWithCell idx header = 
                        Str.replaceAll 
                        (Str.Pattern $ "<" <> header <> ">")
                        (Str.Replacement $ fromMaybe "" $ NEL.index row idx)
                let newLabel = foldrWithIndex replaceWithCell exLabel headerArr
                let newSteps = foldrWithIndex (\idx header -> map $ changeStepLabel $ replaceWithCell idx header) example.steps headerArr

                f newLabel describeType newSteps uniqTags
    
      where
        changeStepLabel :: (String -> String) -> Step -> Step
        changeStepLabel fm (Step step) = Step step{label = fm step.label}

        exLabel = fromMaybe "Scenario: " example.label
