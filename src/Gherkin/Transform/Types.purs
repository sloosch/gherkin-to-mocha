module Gherkin.Transform.Types where

import Data.Maybe (Maybe)
import Data.Newtype (class Newtype)
import Gherkin (Feature)
import Gherkin.Expression.TagExpression as TX

class TransformTo a where
    transformGherkin :: TransformConfiguration -> Feature -> a


newtype TransformConfiguration = TransformConfiguration {
    pathToStepDefintions :: String,
    skipTagExpression :: Maybe TX.Expression,
    onlyTagExpression :: Maybe TX.Expression
}
derive instance newtypeTransformConfiguration :: Newtype TransformConfiguration _
