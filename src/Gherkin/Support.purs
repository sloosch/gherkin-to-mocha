module Gherkin.Support (
    SupportStore,
    createSupportStore,
    RegisterStepTest(..),
    givenStep,
    whenStep,
    thenStep,
    andStep,
    butStep,
    executeGiven,
    executeWhen,
    executeThen,
    executeAnd,
    executeBut,
    registerBeforeFeatureHook,
    registerAfterFeatureHook,
    registerBeforeExampleHook,
    registerAfterExampleHook,
    registerBeforeStepHook,
    registerAfterStepHook,
    excuteHookBeforeFeature,
    excuteHookAfterFeature,
    excuteHookBeforeExample,
    excuteHookAfterExample,
    excuteHookBeforeStep,
    excuteHookAfterStep,
    defineCucumberExpressionToken
) where

import Prelude

import Control.Monad.Except (except, lift, runExceptT, throwError)
import Data.Array (intercalate)
import Data.Array as Arr
import Data.Array.NonEmpty as NEA
import Data.Bifunctor (lmap)
import Data.Either (Either)
import Data.Either as Either
import Data.Foldable (sequence_)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String.Regex (Regex)
import Data.String.Regex as Regex
import Effect (Effect)
import Effect.Ref (Ref)
import Effect.Ref as Ref
import Foreign (Foreign)
import Gherkin (StepType(..), Tag)
import Gherkin.Expression.CucumberExpression as CX
import Gherkin.Expression.CucumberExpression.Token.Support (TokenStore)
import Gherkin.Expression.CucumberExpression.Token.Support as TokenSupport
import Gherkin.Expression.CucumberExpression.Types (CukeExpression, TokenName, TokenRegex, TokenTransformFn, anyToForeign)
import Gherkin.Expression.TagExpression (ExpressionString)
import Gherkin.Expression.TagExpression as TX
import Unsafe.Coerce (unsafeCoerce)

type StepExecuteFn = Array Foreign -> Foreign -> Effect Unit

data StepTest = StepTestWithRegex Regex | StepTestWithExpression (CukeExpression)
instance showStepTest :: Show StepTest where
    show (StepTestWithRegex regex) = "StepTestWithRegex " <> show regex
    show (StepTestWithExpression expr) = "StepTestWithExpression " <> show expr

type StepDesc = {test :: StepTest, step :: StepExecuteFn, type :: StepType}

type SupportHookFn = Array Tag -> Effect Unit

data SupportHookWithTagExpression = SupportHookWithTagExpression (Maybe TX.Expression) SupportHook

data SupportHook =
      HookBeforeFeature SupportHookFn
    | HookAfterFeature SupportHookFn
    | HookBeforeExample SupportHookFn
    | HookAfterExample SupportHookFn
    | HookBeforeStep SupportHookFn
    | HookAfterStep SupportHookFn

newtype HookStore = HookStore (Ref (Array SupportHookWithTagExpression))
newtype StepStore = StepStore (Ref (Array StepDesc))


newtype SupportStore = SupportStore {
    hooks :: HookStore,
    steps :: StepStore,
    tokens :: TokenStore
}

createHookStore :: Effect HookStore
createHookStore = HookStore <$> Ref.new []

createStepStore :: Effect StepStore
createStepStore = StepStore <$> Ref.new []

createSupportStore :: Effect SupportStore
createSupportStore = do
    hookStore <- createHookStore
    stepStore <- createStepStore
    tokenStore <- TokenSupport.createStore

    pure $ SupportStore {
        hooks: hookStore,
        steps: stepStore,
        tokens: tokenStore
    }

matchStepTest :: StepTest -> String -> Maybe (Array Foreign)
matchStepTest (StepTestWithRegex test) label = do
    matches <- Regex.match test label
    pure $ map (unsafeCoerce <<< fromMaybe "") $ Arr.drop 1 $ NEA.toArray $ matches
matchStepTest (StepTestWithExpression expr) label =
    Either.hush $ map anyToForeign <$> CX.match expr label


data RegisterStepTest = RegisterStepTestRegex Regex | RegisterStepTestExpression String

anyStep :: StepType -> SupportStore -> RegisterStepTest -> StepExecuteFn -> Effect (Either String Unit)
anyStep ty (SupportStore {steps: StepStore store, tokens}) test step = runExceptT do
    stepTest <- case test of
        RegisterStepTestRegex regex -> pure $ StepTestWithRegex regex
        RegisterStepTestExpression expr -> do
            definedTokens <- lift $ TokenSupport.allTokens tokens
            except $ StepTestWithExpression <$> CX.compile definedTokens expr

    lift $ Ref.modify_ (Arr.cons {test: stepTest, step, type: ty}) store

givenStep :: SupportStore -> RegisterStepTest -> StepExecuteFn -> Effect (Either String Unit)
givenStep = anyStep Given

whenStep :: SupportStore -> RegisterStepTest -> StepExecuteFn -> Effect (Either String Unit)
whenStep = anyStep When

thenStep :: SupportStore -> RegisterStepTest -> StepExecuteFn -> Effect (Either String Unit)
thenStep = anyStep Then

andStep :: SupportStore -> RegisterStepTest -> StepExecuteFn -> Effect (Either String Unit)
andStep = anyStep And

butStep :: SupportStore -> RegisterStepTest -> StepExecuteFn -> Effect (Either String Unit)
butStep = anyStep But

executeAnyStep :: Maybe StepType -> SupportStore -> String -> Foreign -> Effect (Either String Unit)
executeAnyStep typeToExecute (SupportStore {steps: StepStore store}) label stepDataForeign =
    runExceptT do
        definedSteps <- lift $ Ref.read store 
        case Arr.mapMaybe matchStep definedSteps of
            [] -> 
                throwError $ "No step found for \"" <> label <> "\""
            [{step, matches}] -> 
                lift $ step.step matches stepDataForeign
            more -> 
                let candidates = intercalate ", " $ show <<< _.test <<< _.step <$> more in
                throwError $ "Found multiple step definitions for step \"" <> label <> "\": " <> candidates
        where
        matchStep step = {step, matches: _} <$> matchLabel step
        
        matchLabel {test, type: typ} = case typeToExecute of
            Just tty | tty /= typ -> Nothing
            _ -> matchStepTest test label

executeGiven :: SupportStore -> String -> Foreign -> Effect (Either String Unit)
executeGiven = executeAnyStep (Just Given)

executeWhen :: SupportStore -> String -> Foreign -> Effect (Either String Unit)
executeWhen = executeAnyStep (Just When)

executeThen :: SupportStore -> String -> Foreign -> Effect (Either String Unit)
executeThen = executeAnyStep (Just Then)

executeAnd :: SupportStore -> String -> Foreign -> Effect (Either String Unit)
executeAnd = executeAnyStep Nothing

executeBut :: SupportStore -> String -> Foreign -> Effect (Either String Unit)
executeBut = executeAnyStep Nothing

registerAnyHook :: (SupportHookFn -> SupportHook) -> SupportStore -> Maybe ExpressionString -> SupportHookFn -> Effect (Either String Unit)
registerAnyHook create (SupportStore {hooks: HookStore store}) tagEx f = runExceptT do
    ex <- case tagEx of
        Just ex -> except $ lmap (append $ "Tag Expression \"" <> ex <> "\" did not compile: ") $ Just <$> TX.compile ex
        Nothing -> pure Nothing
    lift $ Ref.modify_ (Arr.cons $ SupportHookWithTagExpression ex $ create f) store

registerBeforeFeatureHook :: SupportStore -> Maybe ExpressionString -> SupportHookFn -> Effect (Either String Unit)
registerBeforeFeatureHook = registerAnyHook HookBeforeFeature

registerAfterFeatureHook :: SupportStore -> Maybe ExpressionString -> SupportHookFn -> Effect (Either String Unit)
registerAfterFeatureHook = registerAnyHook HookAfterFeature

registerBeforeExampleHook :: SupportStore -> Maybe ExpressionString -> SupportHookFn -> Effect (Either String Unit)
registerBeforeExampleHook = registerAnyHook HookBeforeExample

registerAfterExampleHook :: SupportStore -> Maybe ExpressionString -> SupportHookFn -> Effect (Either String Unit)
registerAfterExampleHook = registerAnyHook HookAfterExample

registerBeforeStepHook :: SupportStore -> Maybe ExpressionString -> SupportHookFn -> Effect (Either String Unit)
registerBeforeStepHook = registerAnyHook HookBeforeStep

registerAfterStepHook :: SupportStore -> Maybe ExpressionString -> SupportHookFn -> Effect (Either String Unit)
registerAfterStepHook = registerAnyHook HookAfterStep

excuteAnyHook :: (SupportHook -> Maybe SupportHookFn) -> SupportStore -> SupportHookFn
excuteAnyHook select (SupportStore {hooks: HookStore store}) tags = do
    hooks <- Ref.read store
    sequence_ $ Arr.mapMaybe selectHook hooks

    where
    selectHook :: SupportHookWithTagExpression -> Maybe (Effect Unit)
    selectHook (SupportHookWithTagExpression expr hook) = do
        f <- select hook
        case TX.eval <$> expr <@> tags of
            Just false -> Nothing
            _ -> Just $ f tags

excuteHookBeforeFeature :: SupportStore -> SupportHookFn
excuteHookBeforeFeature = excuteAnyHook case _ of
        (HookBeforeFeature f) -> Just f
        _ -> Nothing

excuteHookAfterFeature :: SupportStore -> SupportHookFn
excuteHookAfterFeature = excuteAnyHook case _ of
        (HookAfterFeature f) -> Just f
        _ -> Nothing

excuteHookBeforeExample :: SupportStore -> SupportHookFn
excuteHookBeforeExample = excuteAnyHook case _ of
        (HookBeforeExample f) -> Just f
        _ -> Nothing

excuteHookAfterExample :: SupportStore -> SupportHookFn
excuteHookAfterExample = excuteAnyHook case _ of
        (HookAfterExample f) -> Just f
        _ -> Nothing

excuteHookBeforeStep :: SupportStore -> SupportHookFn
excuteHookBeforeStep = excuteAnyHook case _ of
        (HookBeforeStep f) -> Just f
        _ -> Nothing

excuteHookAfterStep :: SupportStore -> SupportHookFn
excuteHookAfterStep = excuteAnyHook case _ of
        (HookAfterStep f) -> Just f
        _ -> Nothing

defineCucumberExpressionToken :: SupportStore -> TokenName -> TokenRegex -> TokenTransformFn -> Effect Unit
defineCucumberExpressionToken (SupportStore {tokens: store}) = TokenSupport.defineToken store 
