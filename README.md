### Gherkin to mocha

a webpack loader which will convert gherkin feature files to mocha test suites.

Example webpack config:

```javascript
module.exports = {
    module: {
        rules: [
        {
            test: /\.feature$/,
            exclude: [/node_modules/],
            use: [{
                loader: 'gherkin-to-mocha',
                options: {
                     // the step defintions relative to the feature file
                     // you have to provide an index.js file which imports all your
                     // step definitions and hooks
                    pathToStepDefintions: "../support/step_definitions",
                    // tag expression for features and scenarios to skip
                    skipTagExpression: "@skip or @ignore", 
                    //tag expression for features and scenarios to focus on
                    onlyTagExpression: "@only or @focus" 
                }
            }
            ],
        },
        ],
    }
};
```

Support for given, when, then, and, but steps, including matched arguments and docstring/datatable as last argument:

```javascript
import {Given, When, Then, And, But} from 'gherkin-to-mocha/support'

Given(/^I have foo$/, () => {
    GooglePage.open();
});

When(/^I do bar with "(.+)?"$/, (that) => {
    GooglePage.searchFor(that);
});

Then(/^I can see higgs$/, () => {
    GooglePage.hasResults();
});

And(/^I get a table$/, (table) => {
    console.log(table.headers);
});

But(/^I also want to have this docstring:$/, (docstring) => {
    console.log(docstring);
});
```

Support for Cucumber Expressions:

```javascript
import { defineCucumberExpressionToken } from 'gherkin-to-mocha/support';

// Define your own tokens beside the default int, float, word and string
// IMPORTANT: Define custom tokens always before the steps gets defined (e.g. import custom tokens first)
defineCucumberExpressionToken('color', 'red|green|blue', (s) => new Color(s));

And("I have {word} and see {int} cucumber(s) in the garden/house and it is {color}", (type, num, color) => {
    console.log("You've got " + type + " and can therefore see " + num + " " + color + " cucumbers");
});
```

Support for hooks before and after features, scenarios and steps with tags including inherited tags:
```javascript
import {beforeFeature, beforeScenario, afterFeature, afterScenario, beforeStep, afterStep} from 'gherkin-to-mocha/support'

beforeFeature((tags) => {
    console.log('before feature', tags);
});

afterFeature((tags) => {
    console.log('after feature', tags);
});

beforeScenario((tags) => {
    console.log('before scenario, including tags from the feature', tags);
});

afterScenario((tags) => {
    console.log('after scenario, including tags from the feature', tags);
});

beforeStep((tags) => {
    console.log('befor step, with tags from the scenario and feature', tags);
});

afterStep((tags) => {
    console.log('after step, with tags from the scenario and feature', tags);
});
```
Support for hooks with tag expression:
```javascript
import {beforeTaggedFeature, beforeTaggedScenario, afterTaggedFeature, afterTaggedScenario, beforeTaggedStep, afterTaggedStep} from 'gherkin-to-mocha/support'
beforeTaggedFeature('@this or @that and not (@those or @these)', (tags) => {
    console.log('"complex" tag expressions', tags);
});

afterTaggedScenario('@this and not (@thisTag or @thatTag)', (tags) => {
    console.log('"complex" tag expressions', tags);
});
```
