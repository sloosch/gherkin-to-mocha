import { transformWithForeignConfig } from './dist/Gherkin.Transform.Support';
import { transformToMocha } from './dist/Gherkin.Transform.Mocha';
const { isLeft, fromLeft, fromRight } = require('./dist/Data.Either');
const { getOptions } = require('loader-utils');

module.exports = function(str) {
    const options = getOptions(this);
    const res = transformWithForeignConfig(transformToMocha)(options)(str);
   if (isLeft(res)) {
       throw new Error(fromLeft({})(res));
   } else {
       return fromRight({})(res);
   }
};
